﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parliament.CodeQuality.UnusedCode.Tests.RelatedFunctionsFinderTestClasses
{
    public class Test1
    {
        public interface IBase
        {
            public virtual void Method();
        }

        public class DerivedClass : IBase
        {
            public new void Method()
            {
                
            }
        }
    }
}
