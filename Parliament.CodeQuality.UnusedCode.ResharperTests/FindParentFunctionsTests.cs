﻿
using System.Collections.Generic;
using System.IO;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.Tree;
using JetBrains.ReSharper.TestFramework;
using JetBrains.Util;
using NUnit.Framework;
using System.Linq;
using Parliament.CodeQuality.UnusedCode.Implementation;

namespace Parliament.CodeQuality.UnusedCode.ReSharperTests
{
    public abstract class FindParentFunctionsTestTemplate : BaseTestWithTextControl
    {
        protected abstract void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions);

        protected abstract string TestFileName { get; }

        protected override string RelativeTestDataPath
        {
            get { return Path.GetFullPath(@"TestClasses\RelatedFunctionsFinderTestClasses"); }
        }

        protected override void DoTest(IProject testProject)
        {
            var files = testProject.GetAllProjectFiles().Select(pf => pf.ToSourceFile()).Select(
            sf => sf.GetPsiFile<CSharpLanguage>());
            List<ITypeElement> classes = new List<ITypeElement>();
            files.ForEach(f => f.ProcessChildren<ITypeDeclaration>(td => classes.Add(td.DeclaredElement)));



            foreach (var type in classes)
            {
                if (type.ShortName == "DerivedClass")
                {
                    RelatedFunctionsFinder finder = new RelatedFunctionsFinder();
                    var parentFunctions = finder.GetParentFunctions(
                        type.GetMembers().OfType<IMethod>().Where(m => m.ShortName == "Method").First());
                    MakeAssertionsAboutParentFunctions(parentFunctions);                    
                    return;
                }
            }
            Assert.Fail();
        }
        
        [Test]
        public void Test()
        {

            DoTestFiles(TestFileName);
        }
    }

    public class ParameterlessTest : FindParentFunctionsTestTemplate
    {

        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(1, parentFunctions.Count());
        }

        protected override string TestFileName
        {
            get { return "ParameterlessTest.cs"; }
        }        
    }
    
    public class DifferentSignatureTest : FindParentFunctionsTestTemplate
    {
        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(0, parentFunctions.Count());
        }

        protected override string TestFileName
        {
            get { return "DifferentSignatureTest.cs"; }
        }
    }


    public class HidingTest : FindParentFunctionsTestTemplate
    {

        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(0, parentFunctions.Count());
        }
        
        protected override string TestFileName
        {
            get { return "HidingTest.cs"; }
        }
        
    }

    public class InterfaceTest : FindParentFunctionsTestTemplate
    {

        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(1, parentFunctions.Count());
        }

        protected override string TestFileName
        {
            get { return "InterfaceTest.cs"; }
        }

    }

    public class InterfaceTestWithNew : FindParentFunctionsTestTemplate
    {

        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(1, parentFunctions.Count());
        }

        protected override string TestFileName
        {
            get { return "InterfaceTestWithNew.cs"; }
        }

    }

    public class GrandparentTest : FindParentFunctionsTestTemplate
    {

        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(2, parentFunctions.Count());
        }

        protected override string TestFileName
        { 
            get { return "GrandparentTest.cs"; }
        }

    }

    public class GrandparentWithNewInBetweeenTest : FindParentFunctionsTestTemplate
    {

        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(1, parentFunctions.Count());
            Assert.AreEqual("IntermediateClass", parentFunctions.First().GetContainingType().ShortName);
        }

        protected override string TestFileName
        {
            get { return "GrandparentWithNewInBetweenTest.cs"; }
        }

    }

    public class TwoIdenticalAncestorsTest : FindParentFunctionsTestTemplate
    {

        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(2, parentFunctions.Count());
        }

        protected override string TestFileName
        {
            get { return "TwoIdenticalAncestorsTest.cs"; }
        }

    }

    public class UseOfAliasesTest : FindParentFunctionsTestTemplate {
        protected override void MakeAssertionsAboutParentFunctions(IEnumerable<IFunction> parentFunctions)
        {
            Assert.AreEqual(1, parentFunctions.Count());
        }

        protected override string TestFileName
        {
            get { return "UseOfAliasesTest.cs"; }
        }

    }

}