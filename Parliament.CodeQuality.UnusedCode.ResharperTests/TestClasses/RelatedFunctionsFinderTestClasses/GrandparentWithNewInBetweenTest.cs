﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class GrandparentWithNewInBetweenTest
    {
        public class BaseClass
        {
            public virtual void Method<T>()
            {

            }
        }

        public class IntermediateClass : BaseClass
        {
            public virtual new void Method<T>()
            {

            }
        }

        public class DerivedClass : IntermediateClass
        {
            public override void Method<T>()
            {
                
            }
        }
    }
}
