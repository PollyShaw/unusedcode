﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class GenericTest
    {
        public class BaseClass
        {
            public virtual void Method<T>()
            {
                
            }
        }

        public class DerivedClass : BaseClass
        {
            public void Method()
            {
                
            }
        }
    }
}
