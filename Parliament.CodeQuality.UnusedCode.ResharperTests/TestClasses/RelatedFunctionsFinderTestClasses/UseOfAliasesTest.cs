﻿using System;

namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class UseOfAliasesTest
    {
        public class BaseClass
        {
            public virtual void Method(string s)
            {
                
            }
        }

        public class DerivedClass : BaseClass
        {
            public override void Method(String s)
            {
                
            }
        }
    }
}
