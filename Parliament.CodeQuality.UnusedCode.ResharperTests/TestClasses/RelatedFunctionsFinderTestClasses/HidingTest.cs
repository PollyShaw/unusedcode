﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class HidingTest
    {
        public class BaseClass
        {
            public virtual void Method()
            {
                
            }
        }

        public class DerivedClass : BaseClass
        {
            public new void Method()
            {
                
            }
        }
    }
}
