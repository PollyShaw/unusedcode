﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class InterfaceTest
    {
        public interface IBase
        {
            void Method();
            
        }

        public class DerivedClass : IBase
        {
            public void Method()
            {
                
            }
        }
    }
}
