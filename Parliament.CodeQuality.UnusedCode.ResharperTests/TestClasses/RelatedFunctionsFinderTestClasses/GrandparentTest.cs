﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class GrandparentTest
    {
        public class BaseClass
        {
            public virtual void Method<T>()
            {

            }
        }

        public class IntermediateClass : BaseClass
        {
            public override void Method<T>()
            {

            }
        }

        public class DerivedClass : IntermediateClass
        {
            public override void Method<T>()
            {
                
            }
        }
    }
}
