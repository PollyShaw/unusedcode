﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class DifferentSignatureTest
    {
        public class BaseClass
        {
            public virtual void Method()
            {
                
            }
        }

        public class DerivedClass : BaseClass
        {
            public void Method(int intParam)
            {
                
            }
        }
    }
}
