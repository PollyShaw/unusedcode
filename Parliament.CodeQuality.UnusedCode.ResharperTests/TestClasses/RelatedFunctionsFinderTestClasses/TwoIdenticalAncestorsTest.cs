﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class TwoIdenticalAncestorsTest
    {
        public class BaseClass : IBase
        {
            public virtual void Method(int i)
            {
                
            }
        }

        public interface IBase
        {
            void Method(int i);
        }

        public class DerivedClass : BaseClass, IBase
        {
            public override void Method(int i)
            {
                
            }
        }
    }
}
