﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class ParameterlessTest
    {
        public class BaseClass
        {
            public virtual void Method()
            {
                
            }
        }

        public class DerivedClass : BaseClass
        {
            public override void Method()
            {
                
            }
        }
    }
}
