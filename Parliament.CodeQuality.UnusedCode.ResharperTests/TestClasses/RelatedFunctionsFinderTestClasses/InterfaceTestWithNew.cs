﻿namespace Parliament.CodeQuality.UnusedCode.ReSharperTests.TestClasses.RelatedFunctionsFinderTestClasses
{
    public class InterfaceTestWithNew
    {
        public interface IBase
        {
            void Method();
        }

        public class DerivedClass : IBase
        {
            public new void Method()
            {
                
            }
        }
    }
}
