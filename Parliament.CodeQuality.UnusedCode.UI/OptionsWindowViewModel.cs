﻿// -----------------------------------------------------------------------
// <copyright file="OptionsWindowViewModel.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Parliament.CodeQuality.UnusedCode.Implementation.RecordsToIncludeAndIgnore;
using Parliament.CodeQuality.UnusedCode.Interfaces;
using MessageBox = System.Windows.MessageBox;

namespace Parliament.CodeQuality.UnusedCode.UI
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class OptionsWindowViewModel : INotifyPropertyChanged
    {

        private IFunctionsToAssessSpecifier _functionsToAssessSpecifier;

        public ObservableCollection<ProjectViewModel> Projects { get; private set; }


        private bool? _dialogResult;

        public bool? DialogResult
        {
            get
            {
                return _dialogResult;
            }

            set
            {
                if (_dialogResult != value)
                {
                    _dialogResult = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("DialogResult"));
                    }
                } 
            }
        }


        public void OKClicked()
        {
            List<Project> projectsToInclude = new List<Project>();
            List<Project> projectsToIgnore = new List<Project>();

            foreach (ProjectViewModel projectViewModel in Projects)
            {
                if (projectViewModel.Ignored || projectViewModel.Included)
                {
                    Project project = new Project() { Guid = projectViewModel.Guid };

                    if (projectViewModel.Included)
                    {
                        projectsToInclude.Add(project);
                    }
                    else
                    {
                        projectsToIgnore.Add(project);                        
                    }
                }
            }

            _functionsToAssessSpecifier.SetProjectsToIgnore(projectsToIgnore);
            _functionsToAssessSpecifier.SetProjectsToInclude(projectsToInclude);
            DialogResult = true;
        }

        public OptionsWindowViewModel(IFunctionsToAssessSpecifier functionsToAssessSpecifier)
        {
            this._functionsToAssessSpecifier = functionsToAssessSpecifier;
            this.DialogResult = null;
            this.Projects = new ObservableCollection<ProjectViewModel>();
            
            foreach (Project project in functionsToAssessSpecifier.AllProjects)
            {
                var projectViewModel = new ProjectViewModel() { Guid = project.Guid, Name = project.Name };               
                Projects.Add(projectViewModel);
            }

            foreach (Project project in functionsToAssessSpecifier.ProjectsToInclude)
            {
                var projectToInclude = Projects.FirstOrDefault(p => p.Guid == project.Guid);

                if (projectToInclude != null)
                {
                    projectToInclude.Included = true;
                }
            }

            foreach (Project project in functionsToAssessSpecifier.ProjectsToIgnore)
            {
                var projectToIgnore = Projects.FirstOrDefault(p => p.Guid == project.Guid);

                if (projectToIgnore != null)
                {
                    projectToIgnore.Ignored = true;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
