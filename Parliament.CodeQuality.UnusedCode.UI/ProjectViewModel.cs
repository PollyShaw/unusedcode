using System;
using System.ComponentModel;

namespace Parliament.CodeQuality.UnusedCode.UI
{
    public class ProjectViewModel : INotifyPropertyChanged
    {
        private bool _ignored;
        private bool _included;

        public string Name { get; set; }

        public bool Included
        {
            get { return _included; }
            set
            {
                _included = value;
                if (_ignored && _included)
                {
                    _ignored = false;
                    PropertyChanged(this, new PropertyChangedEventArgs("Ignored"));
                }
            }
        }

        public bool Ignored
        {
            get
            {
                return _ignored;
            }
            set
            {
                _ignored = value;
                if (_ignored && _included)
                {
                    _included = false;
                    PropertyChanged(this, new PropertyChangedEventArgs("Included"));
                }
            }
        }

        public Guid Guid { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}