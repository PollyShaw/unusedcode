﻿using System;
using EnvDTE;

namespace Parliament.CodeQuality.BuildTasks
{
    public class CommandExecutor
    {
        private ILogger _logger;
        private _DTE _visualStudio;

        public CommandExecutor(ILogger logger, _DTE visualStudio)
        {
            // TODO: Complete member initialization
            this._logger = logger;
            this._visualStudio = visualStudio;
        }

        public void Execute(string command, int numberOfRetries, string commandArgs= "")
        {

            // Running the command.  We try a number of times in case there are start-up problems
            _logger.LogMessage("Trying the command {0} with argument {1}", command, commandArgs);
            try
            {

                _visualStudio.TryUntilSuccessOrTimeout(dte => RunCommandSynchronously(command, dte, commandArgs),  numberOfRetries, _logger, typeof(ArgumentException));
            }
            catch (BuildTaskException ex)
            {
                if (_visualStudio.Solution != null)
                {
                    _logger.LogError(
                        "Command failed. To recreate failure, go to Visual Studio, open the solution {0} and run the command {1}.",
                        _visualStudio.Solution.FileName, command);
                }
                else
                {
                    _logger.LogError(
                        "Command failed. To recreate failure, go to Visual Studio and run the command {0}.",
                        command);
                }
            }                             
        }

        private void RunCommandSynchronously(string command, _DTE dte, string commandArgs)
        {
            bool commandHasRun = false;
            string commandGuid = GetCommandGuid(command);

            // Register with the AfterExecute event to get notified when the command has run
            dte.Events.CommandEvents[commandGuid].AfterExecute
                +=
                (guid, id, customIn, customOut) => { commandHasRun = true; };
            dte.ExecuteCommand(command, commandArgs);

            // Wait until the command has run
            while (!commandHasRun)
            {
                System.Threading.Thread.Sleep(1000);
                _logger.LogWarning(
                    "Waiting for command {0} to complete", command);
            }
        }

        private String GetCommandGuid(string commandName)
        {
            foreach (Command command in _visualStudio.Commands)
            {
                if (command.Name == commandName)
                {
                    return command.Guid;                    
                }
            }
            throw new ArgumentException(String.Format("The command {0} cannot be found.", commandName ), "commandName");
        }
    }
}
