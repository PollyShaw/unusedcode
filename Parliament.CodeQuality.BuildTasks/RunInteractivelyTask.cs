﻿// -----------------------------------------------------------------------
// <copyright file="RunInteractivelyTask.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Build.Utilities;

namespace Parliament.CodeQuality.BuildTasks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class RunInteractivelyTask : Task
    {
        #region P/Invoke.
        // ------------------------------------------------------------------



        [Flags]
        enum CreationFlags
        {
            CREATE_SUSPENDED = 0x00000004,
            CREATE_NEW_CONSOLE = 0x00000010,
            CREATE_NEW_PROCESS_GROUP = 0x00000200,
            CREATE_UNICODE_ENVIRONMENT = 0x00000400,
            CREATE_SEPARATE_WOW_VDM = 0x00000800,
            CREATE_DEFAULT_ERROR_MODE = 0x04000000,
        }





        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        struct STARTUPINFO
        {
            public Int32 cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }




        [StructLayout(LayoutKind.Sequential)]
        struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool CreateProcessAsUser(
            IntPtr hToken,
            string lpApplicationName,
            string lpCommandLine,
            ref SECURITY_ATTRIBUTES lpProcessAttributes,
            ref SECURITY_ATTRIBUTES lpThreadAttributes,
            bool bInheritHandles,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref STARTUPINFO lpStartupInfo,
            out PROCESS_INFORMATION lpProcessInformation);

        // ------------------------------------------------------------------

        [DllImport("wtsapi32.dll", SetLastError = true)]
        static extern bool WTSQueryUserToken(UInt32 sessionId, out IntPtr Token);

        [DllImport("kernel32.dll")]

        private static extern uint WTSGetActiveConsoleSessionId();

        [DllImport("userenv.dll", SetLastError = true)]
        static extern bool CreateEnvironmentBlock(out IntPtr lpEnvironment, IntPtr hToken, bool bInherit);
        #endregion

        public String Target { get; set; } 
        public override bool Execute()
        {
            var logger = new Logger(this.Log);

            if (System.Security.Principal.WindowsIdentity.GetCurrent().IsSystem)
            {

                logger.LogMessage("Running {0} interactively", Target);
                String msBuildFileName = System.Reflection.Assembly.GetEntryAssembly().Location;

                IntPtr token;
            
                bool result = WTSQueryUserToken(WTSGetActiveConsoleSessionId(), out token);
                if (!result)
                {
                    int lastError = Marshal.GetLastWin32Error();
                    logger.LogError("Could not create token: {0}", lastError);
                    return false;
                }

                
                SECURITY_ATTRIBUTES processAttributes = new SECURITY_ATTRIBUTES();
                processAttributes.bInheritHandle = -1;
                
                SECURITY_ATTRIBUTES threadAttributes = new SECURITY_ATTRIBUTES();
                threadAttributes.bInheritHandle = -1;
                STARTUPINFO startupInfo = new STARTUPINFO();


                startupInfo.wShowWindow = 1;
                


                PROCESS_INFORMATION processInformation;
                
                               
                IntPtr environmentBlock;

                result = CreateEnvironmentBlock(out environmentBlock, token, false);
                if (!result)
                {
                    int lastError = Marshal.GetLastWin32Error();
                    logger.LogError("Could not create environment block: {0}", lastError);
                    return false;
                }


                string applicationDataDirectory = Path.Combine(Environment.GetEnvironmentVariable("ProgramData"),
                    "Parliament.CodeQuality.BuildTasks");

                if (!Directory.Exists(applicationDataDirectory))
                {
                    Directory.CreateDirectory(applicationDataDirectory);
                }


                // We write the output of the inner task to a file and then 
                // output that file's contents to the standard output
                string outputFile = Path.Combine(applicationDataDirectory, "output.txt");

                logger.LogMessage("Will put output in {0}.", outputFile);


                string args = string.Format("\"{1}\" /t:\"{0}\" >\"{2}\"", this.Target, BuildEngine3.ProjectFileOfTaskNode, outputFile, msBuildFileName);

                logger.LogMessage("Will be running command {0} {1}", msBuildFileName, args);

                string commandFile = Path.Combine(applicationDataDirectory, "command.bat");
                File.WriteAllText(commandFile, string.Format("{0} {1}", msBuildFileName, args));


                result = CreateProcessAsUser(
                                             token,
                                             commandFile,
                                             null,
                                             ref processAttributes,
                                             ref threadAttributes,
                                             true,
                                             (uint)CreationFlags.CREATE_NEW_CONSOLE | (uint) CreationFlags.CREATE_UNICODE_ENVIRONMENT,
                                             environmentBlock,
                                             null,
                                             ref startupInfo,
                                             out processInformation);

                 if (!result)
                {
                    int lastError = Marshal.GetLastWin32Error();
                    logger.LogError("Could not create process: {0}", lastError);
                    return false;
                }

                // Wait for the process to finish before getting the output
                Process process = Process.GetProcesses().FirstOrDefault(p => p.Id == processInformation.dwProcessId);
                if (process != null)
                {
                    process.WaitForExit();
                }
                if (File.Exists(outputFile))
                {
                    logger.LogMessage(File.ReadAllText(outputFile));
                }

            }
            else
            {
                BuildEngine3.BuildProjectFile(BuildEngine3.ProjectFileOfTaskNode, new string[] { this.Target }, null, null);
            }
            return true;
        }
    }
}
