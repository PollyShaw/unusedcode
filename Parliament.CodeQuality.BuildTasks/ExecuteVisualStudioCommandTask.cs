﻿using System;
using EnvDTE;
using Microsoft.Build.Utilities;

namespace Parliament.CodeQuality.BuildTasks
{
    public class ExecuteVisualStudioCommandTask : Task
    {
        public String Solution { get; set; }
        public String Command { get; set; }
        public String NumberOfRetries { get; set; }

        public override bool Execute()
        {
            var logger = new Logger(Log);

            var visualStudioType = System.Type.GetTypeFromProgID("VisualStudio.DTE.10.0");
            var visualStudioInstance = System.Activator.CreateInstance(visualStudioType) as EnvDTE.DTE;
            var commandExecutor = new CommandExecutor(logger, visualStudioInstance);
                
            if (visualStudioInstance == null)
            {
                logger.LogError("Could not create a Visual Studio instance");
                return false;
            }
            try
            {

                visualStudioInstance.TryUntilSuccessOrTimeout(dte => dte.MainWindow.Visible = true, 10, logger);

                commandExecutor.Execute("File.OpenProject", 10, Solution);

                
                Log.LogMessage("Opened solution {0}", Solution);


                
                
               

                // parsing the number of attempts to try

                int maxNumberOfAttempts;
                if (!int.TryParse(NumberOfRetries, out maxNumberOfAttempts))
                {
                    logger.LogWarning("NumberOfRetries value of '{0}' is not a valid integer. Using 10.");
                    maxNumberOfAttempts = 10;
                }

                commandExecutor.Execute(Command, maxNumberOfAttempts);
            }
            finally
            {
                commandExecutor.Execute("File.Exit", 120);
                
            }
            return true;
        }

    }
}
