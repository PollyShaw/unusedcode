﻿namespace Parliament.CodeQuality.BuildTasks
{
    public interface ILogger
    {
        void LogWarning(string message, params object[] args);
        void LogError(string message, params object[] args);
        void LogMessage(string message, params object[] args);
    }
}
