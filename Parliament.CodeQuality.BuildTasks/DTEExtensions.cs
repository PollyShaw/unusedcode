﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Parliament.CodeQuality.BuildTasks
{
    public static class DTEExtensions
    {

        public static void TryUntilSuccessOrTimeout(this EnvDTE._DTE dte,  DTEAction action, int maxNumberOfAttempts, ILogger logger, params Type[] otherExceptionsToAllow )
        {
            int currentAttempt = 0;
            bool hasBeenSuccessful = false;
            while (!hasBeenSuccessful && currentAttempt < maxNumberOfAttempts)
            {
                try
                {
                    currentAttempt++;
                    action(dte);
                    hasBeenSuccessful = true;
                }
                catch (Exception ex)
                {
                    // check whether the exception is a COM exception or is in the list of other allowable exceptions
                    if (ex is COMException || otherExceptionsToAllow.Any(exceptionType => exceptionType.IsInstanceOfType(ex)))
                    {
                        logger.LogMessage("Could not perform action on Visual Studio: {0} Trying again after 1 second",
                                          ex);
                        System.Threading.Thread.Sleep(GetRetryDelay());
                        if (currentAttempt == maxNumberOfAttempts)
                        {
                            throw new BuildTaskException("Failed to perform action on Visual Studio", ex);
                        }
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        public static int GetRetryDelay()
        {
            // check the application settings to see if a value has been supplied
            
            string retryDelayString = ConfigurationManager.AppSettings["Parliament.CodeQuality.BuildTasks.RetryDelay"];
            int retryDelayInt;
            if (int.TryParse(retryDelayString, out retryDelayInt))
            {
                return retryDelayInt;
            }

            return 1000;
        }
    }

    public delegate void DTEAction(EnvDTE._DTE dte);
}
