﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Parliament.CodeQuality.BuildTasks
{
    public class BuildTaskException : ApplicationException
    {
        public BuildTaskException()
        {

        }

        public BuildTaskException(string message)
            : base(message)
        {

        }

        public BuildTaskException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        protected BuildTaskException(SerializationInfo serializationInfo, StreamingContext streamingContext) : 
            base(serializationInfo, streamingContext)
        {

        }
    }
}
