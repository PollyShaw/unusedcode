﻿using Microsoft.Build.Utilities;

namespace Parliament.CodeQuality.BuildTasks
{
    // This class is a wrapper around the BuildTasks TaskLoggingHelper class
    // so that it can be mocked through the interface ILogger
    public class Logger : ILogger 
    {
        private readonly TaskLoggingHelper _taskLogHelper;

        public Logger(TaskLoggingHelper taskLogHelper)
        {
            _taskLogHelper = taskLogHelper;
        }

        public void LogWarning(string message, params object[] args)
        {
            _taskLogHelper.LogWarning(message, args);
        }

        public void LogError(string message, params object[] args)
        {
            _taskLogHelper.LogError(message, args);
        }

        public void LogMessage(string message, params object[] args)
        {
            _taskLogHelper.LogMessage(message, args);
        }
    }
}
