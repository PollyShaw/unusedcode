﻿// -----------------------------------------------------------------------
// <copyright file="FunctionAssessor.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi;
using Parliament.CodeQuality.UnusedCode.Implementation.RecordsToIncludeAndIgnore;
using Parliament.CodeQuality.UnusedCode.Interfaces;

namespace Parliament.CodeQuality.UnusedCode.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FunctionAssessor : IFunctionAssessor, IFunctionsToAssessSpecifier
    {
        private readonly ISolution _solution;
        private List<Project> _projectsToInclude;

        private List<Project> _projectsToIgnore;

        public IEnumerable<Project> ProjectsToInclude
        {
            get { return from p in _projectsToInclude select p; }
        } 

        public IEnumerable<Project> ProjectsToIgnore
        {
            get { return from p in _projectsToIgnore select p; }
        }

        public IEnumerable<Project> AllProjects
        {
            get
            {
                return _solution.GetAllProjects().Select(p => new Project()
                                                                  {
                                                                      Guid = p.Guid,
                                                                      Name = p.Name
                                                                  });
            }
        }

        public void SetProjectsToInclude(IEnumerable<Project> projectsToInclude)
        {
            _projectsToInclude.Clear();
            _projectsToInclude.AddRange(projectsToInclude);
            
        }

        public void SetProjectsToIgnore(IEnumerable<Project> projectsToIgnore)
        {
            _projectsToIgnore.Clear();
            _projectsToIgnore.AddRange(projectsToIgnore);
        }

        public FunctionAssessor(ISolution solution)
        {
            _solution = solution;

            string pathOfUnusedCodeOptions = Path.Combine(solution.SolutionFilePath.Directory.FullPath, "UnusedCodeOptions.xml");
            FunctionAssessor functionAssessor;
            if (File.Exists(pathOfUnusedCodeOptions))
            {
                LoadFromXmlFile(pathOfUnusedCodeOptions);
            }
            else
            {
                GuessProjectsToIncludeAndIgnore();
            }            
        }

        public FunctionAssessor(string locationOfRecordsToIncludeAndIgnore, ISolution solution)
        {
            _solution = solution;
            LoadFromXmlFile(locationOfRecordsToIncludeAndIgnore);
        }

        private void LoadFromXmlFile(string locationOfRecordsToIncludeAndIgnore)
        {
            using (XmlReader reader = new XmlTextReader(new FileStream(locationOfRecordsToIncludeAndIgnore, FileMode.Open, FileAccess.Read)))
            {
                reader.ReadToDescendant("ProjectsToInclude");
                XmlSerializer serializer = new XmlSerializer(typeof(List<Project>), new XmlRootAttribute("ProjectsToInclude"));

                _projectsToInclude = serializer.Deserialize(reader) as List<Project>;
            }

            // Open the xml file again to get the projects to ignore (just so that order doesn't matter)
            using (XmlReader reader = new XmlTextReader(new FileStream(locationOfRecordsToIncludeAndIgnore, FileMode.Open, FileAccess.Read)))
            {
                reader.ReadToDescendant("ProjectsToIgnore");
                XmlSerializer serializer = new XmlSerializer(typeof(List<Project>), new XmlRootAttribute("ProjectsToIgnore"));

                _projectsToIgnore = serializer.Deserialize(reader) as List<Project>;
            }
        }

        private void GuessProjectsToIncludeAndIgnore()
        {
            _projectsToIgnore = new List<Project>();
            _projectsToInclude = new List<Project>();
            foreach (var project in _solution.GetAllProjects())
            {
                if (ProjectIsProbablyToInclude(project))
                {
                    _projectsToInclude.Add(new Project { Guid = project.Guid });
                }
                else if (ProjectIsProbablyToIgnore(project))
                {
                    _projectsToIgnore.Add(new Project { Guid = project.Guid });
                }
            }
        }

        private bool ProjectIsProbablyToIgnore(IProject project)
        {
            var testFrameworks = new[] { "nunit.framework", "machine.specifications", "testStack.bddfy", "mbunit" };
            if (project.GetReferences().Any(r => testFrameworks.Contains(r.Name.ToLower())))
            {
                return true;
            }
            return false;
        }

        private bool ProjectIsProbablyToInclude(IProject project)
        {
            if (project.IsWebApplication)
            {
                return true;
            }
            else
            {
                var buildSettings = project.BuildSettings as IManagedProjectBuildSettings;
                if (buildSettings != null)
                {
                    if (buildSettings.OutputAssemblyFileName.EndsWith(".exe", StringComparison.CurrentCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsToBeIncluded(IFunction function)
        {
            if (this.IsInCompiledAssembly(function))
            {
                return true;
            }            

            IProject project = function.Module.ContainingProjectModule as IProject;
            
            if (project != null)
            {
                return _projectsToInclude.Any(p => p.Guid == project.Guid);
            }
            return false;
        }

        public bool IsToBeIgnored(IFunction function)
        {
            var project = function.Module.ContainingProjectModule as IProject;
            if (project != null)
            {
                return _projectsToIgnore.Any(p => p.Guid == project.Guid);
            }
            return false;
        }

        private bool IsInCompiledAssembly(IFunction function)
        {
            return !function.Module.SourceFiles.Any();
        }

        public void Save(string location)
        {
            using(XmlWriter xmlWriter = new XmlTextWriter(location, null))
            {
                xmlWriter.WriteStartElement("ProjectsToIncludeAndIgnore");

                XmlSerializer serializer = new XmlSerializer(typeof(List<Project>), new XmlRootAttribute("ProjectsToInclude"));

                serializer.Serialize(xmlWriter, _projectsToInclude);

                serializer = new XmlSerializer(typeof(List<Project>), new XmlRootAttribute("ProjectsToIgnore"));

                serializer.Serialize(xmlWriter, _projectsToIgnore);
            }
        }

        public void Save()
        {
            string pathOfUnusedCodeOptions = Path.Combine(_solution.SolutionFilePath.Directory.FullPath, "UnusedCodeOptions.xml");
            Save(pathOfUnusedCodeOptions);
        }
    }    
}
