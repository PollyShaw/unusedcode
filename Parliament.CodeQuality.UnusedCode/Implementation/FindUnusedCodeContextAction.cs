﻿namespace Parliament.CodeQuality.UnusedCode
{
    using System.Linq;

    using JetBrains.ActionManagement;
    using JetBrains.Application;
    using JetBrains.Application.DataContext;
    using JetBrains.Application.Progress;
    using JetBrains.DataFlow;
    using JetBrains.ProjectModel;
    using JetBrains.ReSharper.Features.Common.FindResultsBrowser;
    using JetBrains.ReSharper.Psi.Search;

    using DataConstants = JetBrains.ProjectModel.DataContext.DataConstants;

    [ActionHandler("FindUnusedCodeContextAction")]
    public class FindUnusedCodeContextAction : IActionHandler
    {

        public bool Update(IDataContext context, ActionPresentation presentation, DelegateUpdate nextUpdate)
        {
            // fetch active solution from context
            ISolution solution = context.GetData(DataConstants.SOLUTION);

            // enable this action if there is an active solution, disable otherwise
            return solution != null;
        }

        public void Execute(IDataContext context, DelegateExecute nextExecute)
        {
            
            // Get solution from context in which action is executed
            ISolution solution = context.GetData(DataConstants.SOLUTION);
            if (solution == null)
            {
                return;
            }

            SearchDomainFactory searchDomainFactory = solution.GetComponent<SearchDomainFactory>();

            FindUnusedCodeSearchRequest findUnusedCodeSearchRequest =
                new FindUnusedCodeSearchRequest(context, searchDomainFactory.CreateSearchDomain(solution, false));

            if (findUnusedCodeSearchRequest.NamesOfElementsToSearch().Any()) 
            { 
                var shellLocks = solution.GetComponent<IShellLocks>();

                using (shellLocks.UsingReadLock())
                {

                    var descriptor = new FindUnusedCodeSearchDescriptor(findUnusedCodeSearchRequest);
                    try
                    {
                        descriptor.Search();
                        if (descriptor.Items.Any())
                        {
                            FindResultsBrowser.ShowResults(descriptor);
                        }
                        else
                        {
                            JetBrains.Util.MessageBox.ShowInfo(
                                string.Format("No unused methods found in {0}.",
                                              string.Join(", ",
                                                          findUnusedCodeSearchRequest.NamesOfElementsToSearch().ToArray())),
                                "Find unused code");
                        }
                    }
                    finally
                    {
                        descriptor.LifetimeDefinition.Terminate();
                    }
                }
            } 
            else
            {
                JetBrains.Util.MessageBox.ShowInfo("No code selected to search.");
            }
        }
    }
}
