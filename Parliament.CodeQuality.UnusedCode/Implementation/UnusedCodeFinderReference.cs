namespace Parliament.CodeQuality.UnusedCode
{
    using System.Collections.Generic;
    using System.Linq;

    using JetBrains.ReSharper.Feature.Services.Search;
    using JetBrains.ReSharper.Psi;
    using JetBrains.Util;
    using JetBrains.ReSharper.Feature.Services.Occurences;

    using Parliament.CodeQuality.UnusedCode.Interfaces;

    public class UnusedCodeFinderReference : IUnusedCodeFinderReference
    {
        private readonly IOccurrenceProbe _occurrenceProbe;

        public UnusedCodeFinderReference(IOccurrenceProbe occurrenceProbe)
        {
            this._occurrenceProbe = occurrenceProbe;
        }        

        public IEnumerable<IFunction> GetAssociatedFunctions()
        {
            IDeclaredElement referringMember = _occurrenceProbe.GetAssociatedMember();
            var referencesList = new List<IFunction>();
            if (referringMember != null)
            {
                if (referringMember is IFunction)
                {
                    referencesList.Add(referringMember as IFunction);
                }
                else
                {
                    IProperty referringProperty = referringMember as IProperty;
                    if (referringProperty != null)
                    {
                        referencesList.AddRange(GetSettersOfProperty(referringMember as IProperty));
                    }
                }
            }
            else
            {
                // If the occurrenceProbe is in a type rather than a member (e.g. calling implicit constructor)
                // then return all the methods of the class.
                ITypeElement referringType = _occurrenceProbe.GetAssociatedType();
                if (referringType != null)
                {
                    referencesList.AddRange(this.GetAllNonStaticFunctionsOfClass(referringType));
                }
            }
            return referencesList;
        }

        public bool IsInWebApplication()
        {
            return _occurrenceProbe.IsInWebApplication();
        }

        private static IEnumerable<IFunction> GetSettersOfProperty(IProperty referringProperty)
        {
            if (referringProperty.Setter != null)
            {
                yield return referringProperty.Setter;
            }
            if (referringProperty.Getter != null)
            {
                yield return referringProperty.Getter;
            }
        }
       
        private IEnumerable<IFunction> GetAllNonStaticFunctionsOfClass(ITypeElement typeElement)
        {
            return typeElement.Methods.Where(m => !m.IsStatic)
                .Union(typeElement.Properties.Where(p => !p.IsStatic).SelectMany(p => new IFunction[] { p.Getter, p.Setter }).WhereNotNull());
        }
    }
}