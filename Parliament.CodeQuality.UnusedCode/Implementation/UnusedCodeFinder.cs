﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Parliament.CodeQuality.UnusedCode.Interfaces;

namespace Parliament.CodeQuality.UnusedCode
{
    using System.Xml;

    using JetBrains.Application.DataContext;
    using JetBrains.Application.Progress;
    using JetBrains.ReSharper.Psi;
    using JetBrains.ReSharper.Psi.Dependencies;
    using JetBrains.ReSharper.Psi.Resolve;
    using JetBrains.ReSharper.Psi.Tree;
    using JetBrains.Util;

    public class UnusedCodeFinder
    {        
        private readonly IDependencyFinder _dependencyFinder;

        private readonly IFunctionAssessor _functionAssessor;

        private readonly IFunctionsToAssessProvider _functionsToAssessProvider;

        private readonly IProgressIndicator _progressIndicator;

        private IEnumerable<IFunction> _methodsToAssess;

        private IList<IFunction> _methodsFoundToBeDead;

        private IList<IFunction> _methodsFoundToBeLiving;

        public UnusedCodeFinder(IDependencyFinder dependencyFinder, IFunctionsToAssessProvider functionsToAssessProvider, IProgressIndicator progressIndicator, IFunctionAssessor functionAssessor)
        {
            this._dependencyFinder = dependencyFinder;
            this._functionsToAssessProvider = functionsToAssessProvider;
            _progressIndicator = progressIndicator;
            _functionAssessor = functionAssessor;
            _methodsFoundToBeDead = new List<IFunction>();
            _methodsFoundToBeLiving = new List<IFunction>();
            
        }

        public IEnumerable<IFunction> MethodsToAssess
        {
            get
            {
                if (_methodsToAssess == null)
                {
                    _methodsToAssess = this._functionsToAssessProvider.GetFunctionsToAssess();
                }
                return _methodsToAssess.AsEnumerable();
            }
        }

        // Returns all methods which are unused and not in test assemblies
        public IEnumerable<IFunction> GetMethodsFoundToBeDead()
        {
            return _methodsFoundToBeDead.Intersect(MethodsToAssess).Where(f => !_functionAssessor.IsToBeIgnored(f));
        }

        public IEnumerable<IFunction> GetMethodsFoundToBeLiving()
        {
            return _methodsFoundToBeLiving.Intersect(MethodsToAssess);
        }

        public void Execute()
        {
            _progressIndicator.Start(MethodsToAssess.Count());
            foreach (var method in MethodsToAssess)
            {
                this.MethodIsAlive(method, new List<IFunction>());
            }
            _progressIndicator.Stop();
        }

        private bool MethodIsAlive(IFunction method, List<IFunction> callChain)
        {
            if (this.KnownLiving(method))
            {
                return true;
            }
            if (_functionAssessor.IsToBeIgnored(method))
            {
                this.RegisterAsDead(method);
                return true;
            }

            if (method is IConversionOperator)
            {
                // Find Usages of conversion operators does not work in ReSharper 
                // so we automatically regard them as living. It may be possible to do
                // something more sophisticated.
                this.RegisterAsLiving(method);
                return true;
            }
            
            if (_functionAssessor.IsToBeIncluded(method)) {
                this.RegisterAsLiving(method);
                return true;
            }

            var callingReferences = _dependencyFinder.GetReferences(method);
            if (callingReferences.Any(cr => cr.IsInWebApplication()))
            {
                this.RegisterAsLiving(method);
                return true;
            }

            var callingFunctions = callingReferences.SelectMany(c => c.GetAssociatedFunctions())
                    .Union(_dependencyFinder.GetParentFunctions(method));
            // If this is the default constructor then regard this as used if any non-static methods are used.
            var constructor = method as IConstructor;
            if (constructor != null)
            {
                callingFunctions = callingFunctions.Union(new ConstructorReferenceFinder(constructor).GetCallingFunctions());
            }

            callingFunctions = callingFunctions.Except(callChain).Except(_methodsFoundToBeDead);
            foreach (var callingFunction in callingFunctions)
            {
                List<IFunction> newCallChain = new List<IFunction>(callChain);
                newCallChain.Add(method);
                if (this.MethodIsAlive(callingFunction, newCallChain))
                {
                    this.RegisterAsLiving(method);
                    return true;
                }
            }
            
            this.RegisterAsDead(method);
            return false;
        }        

        private void RegisterAsDead(IFunction method)
        {
            if (!KnownDead(method))
            {
                _methodsFoundToBeDead.Add(method);
                StepProgressIndicator(method);
            }
        }

        private bool KnownDead(IFunction method)
        {
            return _methodsFoundToBeDead.Contains(method);
        }

        private void StepProgressIndicator(IFunction method)
        {
            if (MethodsToAssess.Contains(method))
            {
                _progressIndicator.Advance(1);
            }        
        }

        private void RegisterAsLiving(IFunction method)
        {
            if (!KnownLiving(method)) { 
                this._methodsFoundToBeLiving.Add(method);
                this.StepProgressIndicator(method);
            }
        }

        private bool KnownLiving(IFunction function)
        {
            return this._methodsFoundToBeLiving.Contains(function);
        }
    }
}
