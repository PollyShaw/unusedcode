﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parliament.CodeQuality.UnusedCode.Implementation
{
    using JetBrains.ReSharper.Feature.Services.Occurences;
    using JetBrains.ReSharper.Feature.Services.Search;
    using JetBrains.ReSharper.Psi;

    using Parliament.CodeQuality.UnusedCode.Interfaces;

    public class OccurrenceProbe : IOccurrenceProbe
    {
        private readonly IOccurence _occurrence;

        public OccurrenceProbe(IOccurence occurrence)
        {
            this._occurrence = occurrence;
        }

        public bool IsInWebApplication()
        {
            var refOcc = _occurrence as ReferenceOccurence;
            if (refOcc != null)
            {
                var projectFile = refOcc.GetProjectFile();
                if (projectFile != null)
                {
                    var project = projectFile.GetProject();
                    return project != null && project.IsWebApplication;
                }
            }
            return false;
        }

        public ITypeElement GetAssociatedType()
        {
            if (this._occurrence.TypeElement != null && this._occurrence.TypeElement.IsValid)
            {
                return this._occurrence.TypeElement.GetValidDeclaredElement();
            }
            return null;
        }

        public ITypeMember GetAssociatedMember()
        {
            if (this._occurrence.TypeMember != null && this._occurrence.TypeMember.IsValid)
            {
                return this._occurrence.TypeMember.GetValidDeclaredElement();
            }
            return null;
        }
    }
}
