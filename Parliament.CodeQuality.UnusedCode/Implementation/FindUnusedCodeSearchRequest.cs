﻿using Parliament.CodeQuality.UnusedCode.Implementation;

namespace Parliament.CodeQuality.UnusedCode
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using JetBrains.Application.DataContext;
    using JetBrains.Application.Progress;
    using JetBrains.ProjectModel;
    using JetBrains.ReSharper.Feature.Services.Occurences;
    using JetBrains.ReSharper.Feature.Services.Search;
    using JetBrains.ReSharper.Feature.Services.Search.SearchRequests;
    using JetBrains.ReSharper.Psi;
    using JetBrains.ReSharper.Psi.Search;

    public class FindUnusedCodeSearchRequest : SearchRequest
    {
        private IDataContext _dataContext;
        private ISearchDomain _searchDomain;
        private IEnumerable<IProjectModelElement> _projectModelElements;
        private ISolution _solution;

        public FindUnusedCodeSearchRequest(IDataContext dataContext, ISearchDomain searchDomain)
        {
            this._dataContext = dataContext;
            this._searchDomain = searchDomain;
            this._projectModelElements = dataContext.GetData(JetBrains.ProjectModel.DataContext.DataConstants.PROJECT_MODEL_ELEMENTS);
            this._solution = dataContext.GetData(JetBrains.ProjectModel.DataContext.DataConstants.SOLUTION);
        }
    
        public override ICollection<IOccurence> Search(IProgressIndicator progressIndicator)
        {
            var dependencyFinder =
                new DependencyFinder(_solution, _searchDomain);
            var functionAssessor = new FunctionAssessor(_solution);
            var functionsToAssessProvider = new FunctionsToAssessProvider(_projectModelElements);
            var unusedCodeFinder = new UnusedCodeFinder(dependencyFinder, functionsToAssessProvider, progressIndicator, functionAssessor);
            
            unusedCodeFinder.Execute();
            return new Collection<IOccurence>(unusedCodeFinder.GetMethodsFoundToBeDead().Select(
                    f => new DeclaredElementOccurence(f) as IOccurence).ToList());
        }

        public override string Title
        {
            get
            {
                if (_projectModelElements != null)
                {
                    return "Unused code in " + string.Join(", ", NamesOfElementsToSearch().ToArray());
                }
                else
                {
                    return "Unused code";
                }
            }
        }

        public IEnumerable<string> NamesOfElementsToSearch()
        {
            if (_projectModelElements != null)
            {
                return _projectModelElements.Select(pe => pe.Name);
            }
            else
            {
                return new string[] {};
            }
        }

        public override ISolution Solution
        {
            get
            {
                return this._dataContext.GetData(JetBrains.ProjectModel.DataContext.DataConstants.SOLUTION);                
            }
        }

        public override ICollection SearchTargets
        {
            get
            {
                return new[] { Solution.Name };
            }
        }
    }
}
