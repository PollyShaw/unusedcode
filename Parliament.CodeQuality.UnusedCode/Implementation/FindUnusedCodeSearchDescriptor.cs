﻿// -----------------------------------------------------------------------
// <copyright file="FindUnusedCodeSearchDescriptor.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Parliament.CodeQuality.UnusedCode
{
    using System.Collections.Generic;

    using JetBrains.ReSharper.Feature.Services.Search;
    using JetBrains.ReSharper.Feature.Services.Search.SearchRequests;
    using JetBrains.ReSharper.Features.Common.Occurences;
    using JetBrains.ReSharper.Features.Finding.Search;

    public class FindUnusedCodeSearchDescriptor : SearchDescriptor
    {

        public FindUnusedCodeSearchDescriptor(SearchRequest request)
            : base(request)
        {
        }

        public FindUnusedCodeSearchDescriptor(SearchRequest request, ICollection<IOccurence> results)
            : base(request, results)
        {
        }

        public override string GetResultsTitle(OccurenceSection section)
        {
            return this.Request.Title;
        }
    }
}
