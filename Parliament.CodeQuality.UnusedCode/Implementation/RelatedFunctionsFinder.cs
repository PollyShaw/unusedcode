using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Resolve;
using Parliament.CodeQuality.UnusedCode.Interfaces;

namespace Parliament.CodeQuality.UnusedCode.Implementation
{
    public class RelatedFunctionsFinder : IRelatedFunctionsFinder
    {
        private IEnumerable<IFunction> GetParentFunctions(IFunction function, IList<IDeclaredType> alreadyAssessedTypes )
        {
            List<IDeclaredType> parentTypes = new List<IDeclaredType>(function.GetContainingType().GetSuperTypes().Except(alreadyAssessedTypes));

            // Add the parent types to the list of already assessed types so they are not assessed again if they 
            // appear again in the set of ancestors (this can happen only with interfaces e.g
            // DerivedClass : BaseClass, IBase
            // BaseClass : IBase
            parentTypes.ForEach(t => alreadyAssessedTypes.Add(t));
            
            foreach (var parentType in parentTypes)
            {
                foreach (var method in parentType.GetTypeElement().GetMembers().OfType<IMethod>())
                {
                    if (method.CanBeOverriden() 
                        && method.ShortName == function.ShortName 
                        && FunctionsHaveSameSignature(function, method)
                        && (function.IsOverride || parentType.GetTypeElement() is IInterface))
                    {

                        // Get the parent methods of this method
                        foreach (var parentMethod in GetParentFunctions(method, alreadyAssessedTypes))
                        {
                            yield return parentMethod;
                        }
                        yield return method;
                    }
                }
            }            
        }

        private static bool FunctionsHaveSameSignature(IFunction function1, IFunction function2)
        {
            if (function1 == null)
            {
                throw new ArgumentNullException("function1");
            }
            if (function2 == null)
            {
                throw new ArgumentNullException("function2");
            }
            return function1.GetSignature(EmptySubstitution.INSTANCE).Equals(function2.GetSignature(EmptySubstitution.INSTANCE));
        }

        public IEnumerable<IFunction> GetParentFunctions(IFunction function)
        {
            return GetParentFunctions(function, new List<IDeclaredType>()).ToList();
        }
    }
}