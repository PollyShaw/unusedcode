﻿namespace Parliament.CodeQuality.UnusedCode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using JetBrains.ReSharper.Psi;
    using JetBrains.Util;

    public class ConstructorReferenceFinder
    {
        private readonly IConstructor _constructor;
        private readonly ITypeElement _type;

        public ConstructorReferenceFinder(IConstructor constructor)
        {
            _constructor = constructor;
            _type = _constructor.GetContainingType();
        }

        public IEnumerable<IFunction> GetCallingFunctions()
        {
            IEnumerable<IFunction> returnValue = null;

            if (_constructor != null)
            {
                if (_constructor.IsStatic)
                {
                    // if the constructor is static then all the functions of the class depend on it
                    returnValue = this.GetAllFunctionsOfClass();
                }
                else
                {
                    if (_type.Constructors.Count(c => !c.IsStatic) == 1)
                    {
                        returnValue = this.GetAllNonStaticFunctionsOfClass();
                    }
                }
            }
            return returnValue ?? new IFunction[] { };
        }

        private IEnumerable<IFunction> GetAllFunctionsOfClass()
        {
            return _type.Methods.OfType<IFunction>()
                .Union(_type.Properties
                        .SelectMany(p => new IFunction[] { p.Getter, p.Setter })
                        .WhereNotNull());
        }

        private IEnumerable<IFunction> GetAllNonStaticFunctionsOfClass()
        {
            return GetAllFunctionsOfClass().Where(f => !f.IsStatic);
        }
    }
}
