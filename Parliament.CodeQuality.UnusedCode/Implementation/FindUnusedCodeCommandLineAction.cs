﻿using System;
using System.IO;
using JetBrains.ActionManagement;
using JetBrains.Application.DataContext;
using JetBrains.Application.Progress;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi.Search;
using DataConstants = JetBrains.ProjectModel.DataContext.DataConstants;

namespace Parliament.CodeQuality.UnusedCode.Implementation
{
    [ActionHandler("FindUnusedCodeCommandLineAction")]
    public class FindUnusedCodeCommandLineAction : IActionHandler
    {

        public bool Update(IDataContext context, ActionPresentation presentation, DelegateUpdate nextUpdate)
        {
            // fetch active solution from context
            ISolution solution = context.GetData(DataConstants.SOLUTION);

            // enable this action if there is an active solution, disable otherwise
            return solution != null;
        }

        public void Execute(IDataContext context, DelegateExecute nextExecute)
        {
            
            // Get solution from context in which action is executed
            ISolution solution = context.GetData(DataConstants.SOLUTION);
            if (solution == null)
            {
                return;
            }

            SearchDomainFactory searchDomainFactory = solution.GetComponent<SearchDomainFactory>();

            var dependencyFinder =
                new DependencyFinder(solution, searchDomainFactory.CreateSearchDomain(solution, false));
            var functionsToAssessProvider = new FunctionsToAssessProvider(new []{solution});
            var functionAssessor = new FunctionAssessor(solution);
            var unusedCodeFinder = new UnusedCodeFinder(dependencyFinder, functionsToAssessProvider, NullProgressIndicator.Instance, functionAssessor);

            unusedCodeFinder.Execute();
            string applicationDataDirectory = Path.Combine(Environment.GetEnvironmentVariable("ProgramData"),
                    "Parliament.CodeQuality.UnusedCode");
            if (!Directory.Exists(applicationDataDirectory))
            {
                Directory.CreateDirectory(applicationDataDirectory);
            }



            var outputFileLocation = Path.Combine(applicationDataDirectory, "UnusedMethods.txt");

            var xmlOutputter = new XmlOutputter(outputFileLocation);

            xmlOutputter.Output(unusedCodeFinder.GetMethodsFoundToBeDead());
            
        }
    }
}
