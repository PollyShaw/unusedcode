﻿using Parliament.CodeQuality.UnusedCode.Interfaces;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Application.Progress;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.Search;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Search;

namespace Parliament.CodeQuality.UnusedCode.Implementation
{
    public class DependencyFinder : IDependencyFinder
    {
        private ISearchDomain _searchDomain;

        private ISolution _solution;

        public DependencyFinder(ISolution solution, ISearchDomain searchDomain)
        {
            this._searchDomain = searchDomain;
            this._solution = solution;
        }

        public IEnumerable<IUnusedCodeFinderReference> GetReferences(IFunction function)
        {
            IDeclaredElement elementToFindReferencesTo = function is IAccessor
                                                             ? GetPropertyFromAccessorFunction(function as IAccessor)
                                                             : function;

            var searchResultsConsumer = new SearchResultsConsumer();

            _solution.GetPsiServices().Finder.FindReferences(
                elementToFindReferencesTo,
                this._searchDomain,
                searchResultsConsumer,
                NullProgressIndicator.Instance,
                false);

            return
                searchResultsConsumer.GetOccurences().Select(o => new UnusedCodeFinderReference(new OccurrenceProbe(o)));
        }

        private static IDeclaredElement GetPropertyFromAccessorFunction(IFunction currentFunction)
        {
            var currentAccessor = currentFunction as IAccessor;
            IDeclaredElement elementToFindReferencesTo = currentAccessor.OwnerMember;
            return elementToFindReferencesTo;
        }

        public IEnumerable<IFunction> GetParentFunctions(IFunction function)
        {

            RelatedFunctionsFinder relatedFunctionsFinder = new RelatedFunctionsFinder();

            return relatedFunctionsFinder.GetParentFunctions(function);
        }

    }

}
