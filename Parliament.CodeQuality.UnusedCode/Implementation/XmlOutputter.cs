﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using JetBrains.ReSharper.Psi;

namespace Parliament.CodeQuality.UnusedCode.Implementation
{
    public class XmlOutputter
    {
        private readonly String _fileName;
        
        public XmlOutputter(String fileName)
        {
            _fileName = fileName;
        }

        
        public void Output(IEnumerable<IFunction> methodsToOutput)
        {
            int count = methodsToOutput.Count();

            using (XmlWriter xmlWriter = new XmlTextWriter(_fileName, Encoding.Default))
            {
                xmlWriter.WriteStartElement("UnusedMethods");
                xmlWriter.WriteAttributeString("Count", count.ToString());
                
                foreach (var function in methodsToOutput)
                {
                    xmlWriter.WriteStartElement("Method");
                    xmlWriter.WriteAttributeString("Name", function.ShortName);
                    ITypeElement typeElement = function.GetContainingType();
                    if (typeElement != null)
                    {
                        xmlWriter.WriteAttributeString("Class", typeElement.ShortName);
                        INamespace @namespace = typeElement.GetContainingNamespace();
                        xmlWriter.WriteAttributeString("Namespace", @namespace.QualifiedName);
                    }
                    xmlWriter.WriteEndElement();
                    count++;
                }
                xmlWriter.WriteEndElement();
            }
        }
    }

}
