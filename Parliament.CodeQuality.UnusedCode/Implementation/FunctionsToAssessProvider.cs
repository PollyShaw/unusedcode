using Parliament.CodeQuality.UnusedCode.Interfaces;

namespace Parliament.CodeQuality.UnusedCode
{
    using System.Collections.Generic;
    using System.Linq;

    using JetBrains.Application.DataContext;
    using JetBrains.ProjectModel;
    using JetBrains.ReSharper.Psi;
    using JetBrains.ReSharper.Psi.CSharp;
    using JetBrains.ReSharper.Psi.Tree;
    using JetBrains.Util;

    public class FunctionsToAssessProvider : IFunctionsToAssessProvider
    {
        private IEnumerable<IProjectModelElement> _projectModelElements;

        public FunctionsToAssessProvider(IEnumerable<IProjectModelElement> projectModelElements )
        {
            _projectModelElements = projectModelElements;
        }

        public IEnumerable<IFunction> GetFunctionsToAssess()
        {
            if (_projectModelElements != null)
            {
                List<IPsiSourceFile> sourceFiles = new List<IPsiSourceFile>();
                foreach (IProjectModelElement projectElement in _projectModelElements)
                {
                    sourceFiles.AddRange(GetSourceFilesFromProjectElement(projectElement));
                }
                return GetMethodsToAssessFromSourceFiles(sourceFiles);
            }
            return new IFunction[] { };
        }

        private IEnumerable<IFunction> GetMethodsToAssessFromSourceFiles(List<IPsiSourceFile> sourceFiles)
        {
            List<IFunction> functions = new List<IFunction>();
            sourceFiles.SelectNotNull(s => s.GetPsiFile<CSharpLanguage>()).ForEach(
                s => s.ProcessChildren<IFunctionDeclaration>(fd => functions.Add(fd.DeclaredElement)));
            return functions;
        }


        private IEnumerable<IPsiSourceFile> GetSourceFilesFromProjectElement(IProjectModelElement projectElement)
        {
            if (projectElement is IProject)
            {
                return ((IProject)projectElement).GetAllProjectFiles().SelectMany(psim => psim.ToSourceFiles());
            }
            else if (projectElement is IProjectFile)
            {
                return ((IProjectFile)projectElement).ToSourceFiles();
            }
            else if (projectElement is IProjectFolder)
            {
                return
                    ((IProjectFolder)projectElement).GetSubItems().SelectMany(
                        si => this.GetSourceFilesFromProjectElement(si));
            }
            else if (projectElement is ISolution)
            {
                return ((ISolution)projectElement).GetAllProjects().SelectMany(p => p.GetAllProjectFiles()).SelectMany(pi => pi.ToSourceFiles());
            }
            else return new IPsiSourceFile[] { };

        }

    }
}