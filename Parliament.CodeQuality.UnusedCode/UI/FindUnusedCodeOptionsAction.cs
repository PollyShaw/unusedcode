﻿using System.IO;
using JetBrains.ActionManagement;
using JetBrains.UI.ToolWindowManagement;
using JetBrains.ProjectModel;
using JetBrains.ProjectModel.DataContext;
using Parliament.CodeQuality.UnusedCode.Implementation;
using DataConstants = JetBrains.ProjectModel.DataContext.DataConstants;
using JetBrains.Application.DataContext;


namespace Parliament.CodeQuality.UnusedCode.UI
{
    [ActionHandler("FindUnusedCodeOptionsAction")]
    public class FindUnusedCodeOptionsAction : IActionHandler
    {
        public bool Update(IDataContext context, ActionPresentation presentation, DelegateUpdate nextUpdate)
        {
            // fetch active solution from context
            ISolution solution = context.GetData(DataConstants.SOLUTION);

            // enable this action if there is an active solution, disable otherwise
            return solution != null;
        }

        public void Execute(IDataContext context, DelegateExecute nextExecute)
        {

            // Get solution from context in which action is executed
            ISolution solution = context.GetData(DataConstants.SOLUTION);
            if (solution == null)
            {
                return;
            }

            var functionAssessor = new FunctionAssessor(solution);
            var optionsWindow = new OptionsWindow();
            optionsWindow.DataContext = new OptionsWindowViewModel(functionAssessor);

            if (optionsWindow.ShowDialog() == true)
            {
                functionAssessor.Save();
            }
        }
    }
}
