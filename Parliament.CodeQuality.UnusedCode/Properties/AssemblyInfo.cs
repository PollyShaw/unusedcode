﻿using System.Reflection;

using JetBrains.Application.PluginSupport;
using JetBrains.ActionManagement;
using JetBrains.UI;


[assembly: AssemblyTitle("Parliament.CodeQuality.UnusedCode")]
[assembly: PluginTitle("Unused Code Finder")]
[assembly: PluginVendor("PICT")]
[assembly: PluginDescription("PICT Unused Code Finder")]

// references XML-resource with actions configuration for this plug-in
[assembly: ActionsXml("Parliament.CodeQuality.UnusedCode.Resources.Actions.xml")]

[assembly: AssemblyVersion("0.1.0.0")]
[assembly: AssemblyFileVersion("0.1.0.0")]
