﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.ReSharper.Psi;

namespace Parliament.CodeQuality.UnusedCode.Interfaces
{
    public interface IRelatedFunctionsFinder
    {
        IEnumerable<IFunction> GetParentFunctions(IFunction function);
    }
}
