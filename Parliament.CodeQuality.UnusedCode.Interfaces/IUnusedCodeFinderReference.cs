﻿namespace Parliament.CodeQuality.UnusedCode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using JetBrains.ReSharper.Psi;

    public interface IUnusedCodeFinderReference
    {        
        IEnumerable<IFunction> GetAssociatedFunctions();

        bool IsInWebApplication();
    }
}
