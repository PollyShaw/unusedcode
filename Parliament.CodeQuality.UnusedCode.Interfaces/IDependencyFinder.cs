﻿using System.Collections.Generic;
using JetBrains.ReSharper.Psi;

namespace Parliament.CodeQuality.UnusedCode.Interfaces
{
    public interface IDependencyFinder
    {
        IEnumerable<IUnusedCodeFinderReference> GetReferences(IFunction function);

        IEnumerable<IFunction> GetParentFunctions(IFunction function);

    }
}
