namespace Parliament.CodeQuality.UnusedCode.Interfaces
{
    using JetBrains.ReSharper.Psi;

    public interface IOccurrenceProbe
    {
        bool IsInWebApplication();

        ITypeElement GetAssociatedType();

        ITypeMember GetAssociatedMember();
    }
}