﻿using System.Collections.Generic;
using JetBrains.ReSharper.Psi;

namespace Parliament.CodeQuality.UnusedCode.Interfaces
{
    public interface IFunctionsToAssessProvider
    {
        IEnumerable<IFunction> GetFunctionsToAssess();
    }
}
