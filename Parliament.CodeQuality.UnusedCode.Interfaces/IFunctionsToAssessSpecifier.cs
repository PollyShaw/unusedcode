using System.Collections.Generic;
using Parliament.CodeQuality.UnusedCode.Implementation.RecordsToIncludeAndIgnore;

namespace Parliament.CodeQuality.UnusedCode.Interfaces
{
    public interface IFunctionsToAssessSpecifier
    {
        IEnumerable<Project> ProjectsToInclude { get; }

        IEnumerable<Project> ProjectsToIgnore { get; }

        IEnumerable<Project> AllProjects { get; }

        void SetProjectsToInclude(IEnumerable<Project> projectsToInclude);

        void SetProjectsToIgnore(IEnumerable<Project> projectsToIgnore);
    } 
}