﻿namespace Parliament.CodeQuality.UnusedCode.Implementation.RecordsToIncludeAndIgnore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Project
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
}
