﻿using JetBrains.ReSharper.Psi;

namespace Parliament.CodeQuality.UnusedCode.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IFunctionAssessor
    {
        bool IsToBeIncluded(IFunction function);

        bool IsToBeIgnored(IFunction function);

    }
}
