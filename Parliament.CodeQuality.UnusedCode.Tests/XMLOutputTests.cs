﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using JetBrains.ReSharper.Psi;
using Moq;
using NUnit.Framework;
using Parliament.CodeQuality.UnusedCode.Implementation;
using DefaultValue = Moq.DefaultValue;

namespace Parliament.CodeQuality.UnusedCode.Tests
{
    [TestFixture]
    public class XmlOutputTests
    {
        private XmlOutputter _sut;
        private IEnumerable<IFunction> _methodsToOutput;
        private Mock<IFunction> _method1MockArtisan;
        private Mock<IFunction> _method2MockArtisan;
        private string _testOutputFile;

        [SetUp]
        public void SetUp()
        {
            _testOutputFile = "textoutput.txt";
            _sut = new XmlOutputter(_testOutputFile);

            _method1MockArtisan = new Mock<IFunction>();
            _method1MockArtisan.DefaultValue = DefaultValue.Mock;
            _method2MockArtisan = new Mock<IFunction>();
            _method2MockArtisan.DefaultValue = DefaultValue.Mock;
            
            _methodsToOutput = new[] {_method1MockArtisan.Object, _method2MockArtisan.Object};

            _method1MockArtisan.Setup(f => f.ShortName).Returns("Method1");
            _method2MockArtisan.Setup(f => f.ShortName).Returns("Method2");

            var method1ClassMock = Mock.Get(_method1MockArtisan.Object.GetContainingType());
            var method2ClassMock = Mock.Get(_method2MockArtisan.Object.GetContainingType());

            method1ClassMock.Setup(c => c.ShortName).Returns("Class1");
            method2ClassMock.Setup(c => c.ShortName).Returns("Class2");

            var method1NamespaceMock =  Mock.Get(_method1MockArtisan.Object.GetContainingType().GetContainingNamespace());
            var method2NamespaceMock = Mock.Get(_method2MockArtisan.Object.GetContainingType().GetContainingNamespace());

            method1NamespaceMock.Setup(n => n.QualifiedName).Returns("Test.Namespace1");
            method2NamespaceMock.Setup(n => n.QualifiedName).Returns("Test.Namespace2");


        }

        [Test]
        public void CanCreateXmlOutputter()
        {
            Assert.NotNull(_sut);
        }

        [Test]
        public void XmlOutputterOutputsNamesOfMethods()
        {
            _sut.Output(_methodsToOutput);

            XmlDocument document = new XmlDocument();
            document.Load(_testOutputFile);

            Assert.NotNull(document.SelectSingleNode("/UnusedMethods/Method[@Name='Method1']"));
            Assert.NotNull(document.SelectSingleNode("/UnusedMethods/Method[@Name='Method2']"));
        }

        [Test]
        public void XmlOutputterOutputsNamesOfNamespaces()
        {
            _sut.Output(_methodsToOutput);

            XmlDocument document = new XmlDocument();
            document.Load(_testOutputFile);

            Assert.NotNull(document.SelectSingleNode("/UnusedMethods/Method[@Namespace='Test.Namespace1']"));
            Assert.NotNull(document.SelectSingleNode("/UnusedMethods/Method[@Namespace='Test.Namespace2']"));
        }

        [Test]
        public void XmlOutputterOutputsNamesOfClasses()
        {
            _sut.Output(_methodsToOutput);

            XmlDocument document = new XmlDocument();
            document.Load(_testOutputFile);

            Assert.NotNull(document.SelectSingleNode("/UnusedMethods/Method[@Class='Class1']"));
            Assert.NotNull(document.SelectSingleNode("/UnusedMethods/Method[@Class='Class2']"));
        }

        [Test]
        public void XmlOutputterOutputsCountOfUnusedMethods()
        {
            _sut.Output(_methodsToOutput);

            XmlDocument document = new XmlDocument();
            document.Load(_testOutputFile);

            Assert.AreEqual("2", document.SelectSingleNode("/UnusedMethods/@Count").Value);
        }

    }
}
