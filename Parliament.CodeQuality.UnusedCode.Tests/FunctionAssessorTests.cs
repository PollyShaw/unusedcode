﻿using System.IO;
using FluentAssertions;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi;
using JetBrains.Util;
using Moq;
using NUnit.Framework;
using Parliament.CodeQuality.UnusedCode.Implementation;
using Parliament.CodeQuality.UnusedCode.Implementation.RecordsToIncludeAndIgnore;
using DefaultValue = Moq.DefaultValue;

namespace Parliament.CodeQuality.UnusedCode.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [TestFixture]
    public class FunctionAssessorTests
    {
        private FunctionAssessor _sut;
        private Guid _projectToIgnoreGuid;
        private Guid _projectToIncludeGuid;
        private Mock<IFunction> _includedFunctionMockArtisan;
        private Mock<IFunction> _ignoredFunctionMockArtisan;
        private Mock<ISolution> _solutionMockArtisan;



        [SetUp]
        public void SetUp()
        {
            _solutionMockArtisan = new Mock<ISolution>();
            _sut = new FunctionAssessor("ProjectsToInclude.xml", _solutionMockArtisan.Object);
            _projectToIncludeGuid = new Guid("{BD504E0A-DD73-4521-BA6C-59667DDF4D3F}");
            _projectToIgnoreGuid = new Guid("{BE329110-4985-4050-A836-66B134E23301}");
            _includedFunctionMockArtisan = BuildIncludedFunctionMockArtisan();
            _ignoredFunctionMockArtisan = BuildIgnoredFunctionMockArtisan();
        }

        [Test]
        public void CanBeCreated()
        {
            _sut.Should().NotBeNull();
        }

        [Test]
        public void ReadsProjectToIncludeGuidFromFile()
        {
            _sut.ProjectsToInclude.Any(p => p.Guid == _projectToIncludeGuid).Should().BeTrue();
        }

        [Test]
        public void ReadsProjectToIgnoreGuidFromFile()
        {
            _sut.ProjectsToIgnore.Any(p => p.Guid == _projectToIgnoreGuid).Should().BeTrue();
        }

        [Test]
        public void IsToBeIncludedReturnsTrueIfFunctionIsInIncludedAssembly()
        {
            _sut.IsToBeIncluded(_includedFunctionMockArtisan.Object).Should().BeTrue();
        }

        [Test]
        public void IsToBeIgnoredReturnsTrueIfFunctionIsInIgnoredAssembly()
        {
            _sut.IsToBeIgnored(_ignoredFunctionMockArtisan.Object).Should().BeTrue();
        }

        [Test]
        public void SaveProducesFile()
        {
            var now = DateTime.Now;
            _sut.Save("TestSaveDestination.xml");
            File.GetLastWriteTime("TestSaveDestination.xml").CompareTo(now).Should().Be(1);
        }

        [Test]
        public void SaveSavesProjectsToInclude()
        {
            _sut.Save("TestSaveDestination.xml");

            var restoredFunctionAssessor = new FunctionAssessor("TestSaveDestination.xml", _solutionMockArtisan.Object);

            restoredFunctionAssessor.ProjectsToInclude.Select(p => p.Guid)
                .Should().BeEquivalentTo(new[] { _projectToIncludeGuid });                
        }

        [Test]
        public void SaveSavesProjectsToIgnore()
        {
            _sut.Save("TestSaveDestination.xml");

            var restoredFunctionAssessor = new FunctionAssessor("TestSaveDestination.xml", _solutionMockArtisan.Object);

            restoredFunctionAssessor.ProjectsToIgnore.Select(p => p.Guid)
                .Should().BeEquivalentTo(new[] { _projectToIgnoreGuid });
            
        }

        [Test]
        public void AllProjectsGetsAllProjectsFromTheSolution()
        {
            var project1MockArtisan = new Mock<IProject>();
            project1MockArtisan.SetupGet(p => p.Guid).Returns(_projectToIgnoreGuid);
            var project2MockArtisan = new Mock<IProject>();
            project2MockArtisan.SetupGet(p => p.Guid).Returns(_projectToIncludeGuid);

            _solutionMockArtisan.Setup(s => s.GetAllProjects()).Returns(new IProject[]
                                                                            {
                                                                                project1MockArtisan.Object,
                                                                                project2MockArtisan.Object
                                                                            });

            _sut = new FunctionAssessor("ProjectsToInclude.xml", _solutionMockArtisan.Object);

            _sut.AllProjects.Should().Contain(p => p.Guid == _projectToIgnoreGuid)
                .And.Contain(p => p.Guid == _projectToIncludeGuid)
                .And.HaveCount(2);

        }

        [Test]
        public void AllProjectsGetsAllProjectsWithNamesFromTheSolution()
        {
            var project1MockArtisan = new Mock<IProject>();
            project1MockArtisan.SetupGet(p => p.Guid).Returns(_projectToIgnoreGuid);
            project1MockArtisan.SetupGet(p => p.Name).Returns("Project 1");
            var project2MockArtisan = new Mock<IProject>();
            project2MockArtisan.SetupGet(p => p.Guid).Returns(_projectToIncludeGuid);
            project2MockArtisan.SetupGet(p => p.Name).Returns("Project 2");
            
            _solutionMockArtisan.Setup(s => s.GetAllProjects()).Returns(new IProject[]
                                                                            {
                                                                                project1MockArtisan.Object,
                                                                                project2MockArtisan.Object
                                                                            });

            _sut = new FunctionAssessor("ProjectsToInclude.xml", _solutionMockArtisan.Object);

            _sut.AllProjects.Should().Contain(p => p.Name == "Project 1")
                .And.Contain(p => p.Name == "Project 2")
                .And.HaveCount(2);
        }

        [Test]
        public void SetProjectsToIncludeRepopulatesProjectsToInclude()
        {
            _sut = new FunctionAssessor("ProjectsToInclude.xml", _solutionMockArtisan.Object);

            _sut.SetProjectsToInclude(new[] { new Project() { Guid = _projectToIncludeGuid } });

            _sut.ProjectsToInclude.Should().Contain(p => p.Guid == _projectToIncludeGuid).And.HaveCount(1);
        }

        [Test]
        public void SetProjectsToIgnoreRepopulatesProjectsToIgnore()
        {
            _sut = new FunctionAssessor("ProjectsToInclude.xml", _solutionMockArtisan.Object);

            _sut.SetProjectsToIgnore(new[] { new Project() { Guid = _projectToIgnoreGuid } });

            _sut.ProjectsToIgnore.Should().Contain(p => p.Guid == _projectToIgnoreGuid).And.HaveCount(1);
        }

        [Test]
        public void SaveSavesInTheLocationOfTheSolution()
        {

            var now = DateTime.Now;
            var solutionFilePath = new FileSystemPath("here.sln");

            _solutionMockArtisan.Setup(s => s.SolutionFilePath).Returns(solutionFilePath);

            _sut = new FunctionAssessor("ProjectsToInclude.xml", _solutionMockArtisan.Object);

            _sut.Save();

            File.GetLastWriteTime("UnusedCodeOptions.xml").Should().BeAfter(now);
        }

        [Test]
        public void WhenIsCreatedOnlyWithTheSolutionReadsFromTheLocationOfTheSolution()
        {

            var now = DateTime.Now;
            var solutionFilePath = new FileSystemPath("here.sln");

            File.Copy("ProjectsToInclude.xml", "UnusedCodeOptions.xml", true);

            _solutionMockArtisan.Setup(s => s.SolutionFilePath).Returns(solutionFilePath);

            _sut = new FunctionAssessor(_solutionMockArtisan.Object);

            _sut.ProjectsToInclude.Any(p => p.Guid == _projectToIncludeGuid).Should().BeTrue();
            _sut.ProjectsToIgnore.Any(p => p.Guid == _projectToIgnoreGuid).Should().BeTrue();
        }
        

        private Mock<IFunction> BuildIncludedFunctionMockArtisan()
        {
            Mock<IFunction> includedFunctionMockArtisan = new Mock<IFunction>();
            includedFunctionMockArtisan.DefaultValue = DefaultValue.Mock;
            Mock<IPsiModule> moduleMockArtisan = Mock.Get(includedFunctionMockArtisan.Object.Module);

            Mock<IProject> projectMockArtisan = new Mock<IProject>();
            moduleMockArtisan.Setup(m => m.ContainingProjectModule).Returns(projectMockArtisan.Object);
            projectMockArtisan.Setup(p => p.Guid).Returns(_projectToIncludeGuid);

            // Ensure that the module is not regarded as a compiled assembly
            moduleMockArtisan.Setup(m => m.SourceFiles).Returns(
                new List<IPsiSourceFile>(new[] {Mock.Of<IPsiSourceFile>()}));
            return includedFunctionMockArtisan;
        }

        private Mock<IFunction> BuildIgnoredFunctionMockArtisan()
        {
            Mock<IFunction> ignoredFunctionMockArtisan = new Mock<IFunction>();
            ignoredFunctionMockArtisan.DefaultValue = DefaultValue.Mock;
            Mock<IPsiModule> moduleMockArtisan = Mock.Get(ignoredFunctionMockArtisan.Object.Module);

            Mock<IProject> projectMockArtisan = new Mock<IProject>();
            moduleMockArtisan.Setup(m => m.ContainingProjectModule).Returns(projectMockArtisan.Object);
            projectMockArtisan.Setup(p => p.Guid).Returns(_projectToIgnoreGuid);

            // Ensure that the module is not regarded as a compiled assembly
            moduleMockArtisan.Setup(m => m.SourceFiles).Returns(
                new List<IPsiSourceFile>(new[] { Mock.Of<IPsiSourceFile>() }));
            return ignoredFunctionMockArtisan;
        }
    }    
}
