﻿using Parliament.CodeQuality.UnusedCode.Interfaces;

namespace Parliament.CodeQuality.UnusedCode.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;

    using JetBrains.Application.DataContext;
    using JetBrains.Application.Progress;
    using JetBrains.ReSharper.Psi;

    using Moq;

    using NUnit.Framework;
    
    [TestFixture]
    public class UnusedCodeFinderTests
    {
        private UnusedCodeFinder _sut;

        protected IDataContext _mockDataContext;

        protected IDependencyFinder _mockDependencyFinder;

        protected IFunctionsToAssessProvider _mockFunctionsToAssessProvider;

        private Mock<IDataContext> _dataContextMockArtisan;

        protected Mock<IDependencyFinder> _dependencyFinderMockArtisan;

        private Mock<IFunctionAssessor> _functionAssessorMockArtisan;

        private IFunctionAssessor _mockFunctionAssessor;

        private Mock<IFunctionsToAssessProvider> _functionsToAssessMockArtisan;

        protected IFunction[] _mockFunctions;

        protected List<int> _functionIndexesInCallingAssemblies;

        protected List<int> _functionIndexesInTestAssemblies;

        protected List<int> _functionIndexesToAssess;

        protected List<int>[] _callingFunctions;

        protected IEnumerable<IFunction> FunctionsToAssess
        {
            get
            {
                return this._functionIndexesToAssess.Select(i => this._mockFunctions[i]);
            }
        }

        private IEnumerable<IFunction> FunctionsInCallingAssemblies
        {
            get
            {
                return this._functionIndexesInCallingAssemblies.Select(i => this._mockFunctions[i]);
            }
        }

        private IEnumerable<IFunction> FunctionsInTestLibraries
        {
            get
            {
                return this._functionIndexesInTestAssemblies.Select(i => this._mockFunctions[i]);
            }
        }

        [SetUp]
        public virtual void SetUp()
        {
            this._functionIndexesInCallingAssemblies = new List<int>();

            this._functionIndexesInTestAssemblies = new List<int>();

            this._functionIndexesToAssess = new List<int>();
            
            this._callingFunctions = new List<int>[10];
            

            this._dataContextMockArtisan = new Mock<IDataContext>();
            this._mockDataContext = this._dataContextMockArtisan.Object;

            _functionAssessorMockArtisan = new Mock<IFunctionAssessor>();

            this._mockFunctionAssessor = _functionAssessorMockArtisan.Object;

            this._functionAssessorMockArtisan.Setup(df => df.IsToBeIncluded(It.IsAny<IFunction>())).Returns<IFunction>(f => Enumerable.Contains(this.FunctionsInCallingAssemblies, f));
            this._functionAssessorMockArtisan.Setup(df => df.IsToBeIgnored(It.IsAny<IFunction>())).Returns<IFunction>(f => Enumerable.Contains(this.FunctionsInTestLibraries, f));
            

            this._dependencyFinderMockArtisan = new Mock<IDependencyFinder>();
            this._dependencyFinderMockArtisan.Setup(df => df.GetReferences(It.IsAny<IFunction>())).Returns<IFunction>(
                f =>
                    {
                            int index = Array.IndexOf(this._mockFunctions, f);
                            List<IUnusedCodeFinderReference> references = new List<IUnusedCodeFinderReference>();
                            foreach (int callingFunctionIndex in this._callingFunctions[index])
                            {
                                var reference = new Mock<IUnusedCodeFinderReference>().Object;
                                Mock.Get(reference).Setup(r => r.GetAssociatedFunctions()).Returns(
                                    new[] { this._mockFunctions[callingFunctionIndex] });
                                references.Add(reference);
                            }
                            return references;
                        });
        
            this._mockDependencyFinder = this._dependencyFinderMockArtisan.Object;

            this._functionsToAssessMockArtisan = new Mock<IFunctionsToAssessProvider>();
            this._mockFunctionsToAssessProvider = this._functionsToAssessMockArtisan.Object;
            this._functionsToAssessMockArtisan.Setup(f => f.GetFunctionsToAssess()).Returns(
                this.FunctionsToAssess);

            this._mockFunctions = new IFunction[10];
            for (int i = 0; i < 10; i++)
            {
                this._mockFunctions[i] = new Mock<IFunction>().Object;
                this._callingFunctions[i] = new List<int>();
            }
        
            this._sut = new UnusedCodeFinder(this._mockDependencyFinder, 
                this._mockFunctionsToAssessProvider, 
                NullProgressIndicator.Instance, 
                this._mockFunctionAssessor);
        }

        [Test]
        public void CanBeCreated()
        {
            this._sut.Should().NotBeNull();
        }

        [Test]
        public void HasMethodsToAssessEnumerable()
        {
            this._functionIndexesToAssess.AddRange(new[] { 0, 1 });
            this._sut.MethodsToAssess.Should().BeEquivalentTo(this.FunctionsToAssess.Select(f => f as object));
        }

        [Test]
        public void HasMethodsFoundToBeDeadEnumerable()
        {
            var deadMethods = this._sut.GetMethodsFoundToBeDead();
            deadMethods.Should().BeEmpty();
        }

        [Test]
        public void HasMethodsFoundToBeLivingEnumerable()
        {
            var livingMethods = this._sut.GetMethodsFoundToBeLiving();
            livingMethods.Should().BeEmpty();
        }

        [Test]
        public void WhenMethodIsInCallingLibrariesItIsAssessedAsLiving()
        {
            this._functionIndexesToAssess.Add(0);
            this._functionIndexesInCallingAssemblies.Add(0);
            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[0]);
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();
        }

        [Test]
        public void WhenMethodIsInTestLibrariesItIsNeitherDeadNorAlive()
        {

            this._functionIndexesToAssess.Add(0);
            this._functionIndexesInTestAssemblies.Add(0);

            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().BeEmpty();
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();            
        }
        
        [Test]
        public void WhenMethodHasNoCallersItIsAssessedAsDead()
        {
            this._functionIndexesToAssess.AddRange(new[] { 0, 1 });
            this._functionIndexesInCallingAssemblies.Add(0);
            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[0]);
            this._sut.GetMethodsFoundToBeDead().Should().Contain(this._mockFunctions[1]);            
        }

        [Test]
        public void WhenMethodIsCalledByAliveMethodItIsAssessedAsAlive()
        {
            this._functionIndexesToAssess.AddRange(new[] { 0, 1 });
            this._functionIndexesInCallingAssemblies.Add(1);

            this._callingFunctions[0].Add(1);
                        
            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[0]).And.Contain(this._mockFunctions[1]);
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();
        }

        [Test]
        public void WhenMethodIsOnlyCalledByADeadMethodItIsAssessedAsDead()
        {
            this._functionIndexesToAssess.AddRange(new[] { 0, 1 });
            this._functionIndexesInTestAssemblies.Add(1);


            this._callingFunctions[1].Add(0);
            
            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().BeEmpty();
            this._sut.GetMethodsFoundToBeDead().Should().Contain(this._mockFunctions[0]);
        }

        [Test]
        public void WhenRecurringMethodIsUncalledItIsAssessedAsDead()
        {
            this._functionIndexesToAssess.AddRange(new[] { 0, 1 });
            this._functionIndexesInCallingAssemblies.Add(1);

            this._callingFunctions[0].Add(0);
            
            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[1]);
            this._sut.GetMethodsFoundToBeDead().Should().Contain(this._mockFunctions[0]);
        }

        [Test]
        public void WhenMutuallyRecurringMethodsAreUncalledTheyAreAssessedAsDead()
        {
            this._functionIndexesToAssess.AddRange(new[] { 0, 1, 2 });
            this._functionIndexesInCallingAssemblies.Add(2);

            this._callingFunctions[0].Add(1);
            this._callingFunctions[1].Add(0);
            
            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[2]);
            this._sut.GetMethodsFoundToBeDead().Should().Contain(this._mockFunctions[0]).And.Contain(this._mockFunctions[1]);
            
        }

        [Test]
        public void WhenAMethodHasAParentMethodWhichIsInACallingLibraryItIsAssessedAsAlive()
        {
            this._functionIndexesToAssess.AddRange(new[] { 0, 1 });
            this._functionIndexesInCallingAssemblies.Add(1);
            this._dependencyFinderMockArtisan.Setup(f => f.GetParentFunctions(this._mockFunctions[0])).Returns(
                new IFunction[] { this._mockFunctions[1] });

            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[1]).And.Contain(this._mockFunctions[0]);
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();            
        }

        [Test]
        public void WhenAMethodHasAParentMethodWhichIsIncludedItIsAssessedAsAlive()
        {
            this._functionIndexesToAssess.Add(0);
            this._functionAssessorMockArtisan.Setup(f => f.IsToBeIncluded(this._mockFunctions[1])).Returns(true);
            this._dependencyFinderMockArtisan.Setup(f => f.GetParentFunctions(this._mockFunctions[0])).Returns(
                new IFunction[] { this._mockFunctions[1] });

            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().NotContain(this._mockFunctions[1]).And.Contain(this._mockFunctions[0]);
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();
        }

        [Test]
        public void WhenAMethodHasAParentMethodWhichIsIncludedButIsInATestAssemblyItIsNotAlive()
        {
            this._functionIndexesToAssess.Add(0);
            this._functionIndexesInTestAssemblies.Add(0);
            this._functionAssessorMockArtisan.Setup(f => f.IsToBeIncluded(this._mockFunctions[1])).Returns(true);
            this._dependencyFinderMockArtisan.Setup(f => f.GetParentFunctions(this._mockFunctions[0])).Returns(
                new IFunction[] { this._mockFunctions[1] });

            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().BeEmpty();
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();
        }

        [Test]
        public void WhenAMethodHasAReferenceWhichIsCalledByCallingLibraryItIsAssessedAsAlive()
        {
            Mock<IUnusedCodeFinderReference> referenceMockArtisan = new Mock<IUnusedCodeFinderReference>();
            referenceMockArtisan.Setup(r => r.IsInWebApplication()).Returns(true);

            this._functionIndexesToAssess.Add(0);
            this._dependencyFinderMockArtisan.Setup(df => df.GetReferences(this._mockFunctions[0])).Returns(
                         new[] { referenceMockArtisan.Object });

            var reference = this._mockDependencyFinder.GetReferences(this._mockFunctions[0]).First();
            Mock.Get(reference).Setup(r => r.IsInWebApplication()).Returns(true);

            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[0]);
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();
        }

        [Test]
        public void DeadCallingMethodsNotInSetToAssessShouldNotAppearInDeadMethodList()
        {
            
            this._functionIndexesToAssess.Add(0);
            this._callingFunctions[0].Add(1);
                        
            this._sut.Execute();

            this._sut.GetMethodsFoundToBeLiving().Should().BeEmpty();
            this._sut.GetMethodsFoundToBeDead().Should().Contain(this._mockFunctions[0]).And.NotContain(this._mockFunctions[1]);
        }        

        [Test]
        public void ConversionOperatorsShouldBeAssessedAsUsed()
        {
            Mock<IConversionOperator> conversionOperatorMockArtisan = new Mock<IConversionOperator>();
            _mockFunctions[0] = conversionOperatorMockArtisan.Object;

            _functionIndexesToAssess.Add(0);

            this._sut.Execute();
            this._sut.GetMethodsFoundToBeLiving().Should().Contain(this._mockFunctions[0]);
            this._sut.GetMethodsFoundToBeDead().Should().BeEmpty();

        }
    }
}
