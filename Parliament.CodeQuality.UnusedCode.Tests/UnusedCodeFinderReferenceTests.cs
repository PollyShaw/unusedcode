﻿namespace Parliament.CodeQuality.UnusedCode.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using FluentAssertions;
    using JetBrains.ReSharper.Psi;

    using Moq;

    using NUnit.Framework;

    using Parliament.CodeQuality.UnusedCode.Interfaces;

    using DefaultValue = Moq.DefaultValue;

    [TestFixture]
    public class UnusedCodeFinderReferenceTests
    {
        private UnusedCodeFinderReference _sut;

        private Mock<IOccurrenceProbe> _occurrenceProbeMockArtisan;

        [SetUp]
        public void SetUp()
        {
            this._occurrenceProbeMockArtisan = new Mock<IOccurrenceProbe>();
            this._occurrenceProbeMockArtisan.DefaultValue = DefaultValue.Mock;
            _sut = new UnusedCodeFinderReference(this._occurrenceProbeMockArtisan.Object);
        }

        [Test]
        public void GetAssociatedFunctionsShouldReturnTheFunctionWhenOccurenceIsAMethod()
        {
            var methodMockArtisan = new Mock<IMethod>();
            
            this._occurrenceProbeMockArtisan.Setup(o => o.GetAssociatedMember()).Returns(methodMockArtisan.Object);
            
            _sut.GetAssociatedFunctions().Should().Contain(methodMockArtisan.Object);
        }
     
        [Test]
        public void GetAssociatedFunctionsShouldReturnThePropertyAccessorsWhenOccurrenceIsAProperty()
        {
            var propertyMockArtisan = new Mock<IProperty>();
            propertyMockArtisan.DefaultValue = DefaultValue.Mock;
            
            this._occurrenceProbeMockArtisan.Setup(o => o.GetAssociatedMember()).Returns(propertyMockArtisan.Object);

            _sut.GetAssociatedFunctions().Should().Contain(propertyMockArtisan.Object.Getter)
                .And.Contain(propertyMockArtisan.Object.Setter);                 
        }

        [Test]
        public void GetAssociatedFunctionsShouldReturnAllNonStaticMethodsOfTheTypeWhenOccurrenceIsAType()
        {
            var typeMockArtisan = new Mock<ITypeElement>();
            
            this._occurrenceProbeMockArtisan.Setup(o => o.GetAssociatedMember()).Returns(null as ITypeMember);
            this._occurrenceProbeMockArtisan.Setup(o => o.GetAssociatedType()).Returns(typeMockArtisan.Object);

            var staticMethodMockArtisan = new Mock<IMethod>();
            staticMethodMockArtisan.Setup(m => m.IsStatic).Returns(true);

            var nonStaticMethodMockArtisan = new Mock<IMethod>();
            nonStaticMethodMockArtisan.Setup(m => m.IsStatic).Returns(false);

            typeMockArtisan.Setup(t => t.Methods).Returns(
                new[] { staticMethodMockArtisan.Object, nonStaticMethodMockArtisan.Object });


            _sut.GetAssociatedFunctions().Should().Contain(nonStaticMethodMockArtisan.Object)
                .And.NotContain(staticMethodMockArtisan.Object);
        }

        [Test]
        public void IsInWebApplicationShouldReturnTrueIfMethodInOccurrenceProbeReturnsTrue()
        {
            this._occurrenceProbeMockArtisan.Setup(o => o.IsInWebApplication()).Returns(true);
            _sut.IsInWebApplication().Should().BeTrue();
        }

        [Test]
        public void IsInWebApplicationShouldReturnFalseIfMethodInOccurrenceProbeReturnsFalse()
        {
            this._occurrenceProbeMockArtisan.Setup(o => o.IsInWebApplication()).Returns(false);
            _sut.IsInWebApplication().Should().BeFalse();
        }
    }
}
