﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parliament.CodeQuality.UnusedCode.Tests
{
    using FluentAssertions;

    using JetBrains.ReSharper.Psi;

    using Moq;

    using NUnit.Framework;

    using DefaultValue = Moq.DefaultValue;

    [TestFixture]
    public class ConstructorReferenceFinderTests 
    {
        private ConstructorReferenceFinder _sut;

        private Mock<IConstructor> _constructorMockArtisan;

        private IConstructor _mockConstructor;

        [SetUp]
        public void SetUp()
        {
            _constructorMockArtisan = new Mock<IConstructor>();
            _constructorMockArtisan.Setup(s => s.IsStatic).Returns(false);
            _constructorMockArtisan.DefaultValue = DefaultValue.Mock;

            _mockConstructor = _constructorMockArtisan.Object;
            _sut = new ConstructorReferenceFinder(_mockConstructor);
        }

        [Test]
        public void DefaultConstructorsShouldBeAssessedAsLivingIfAnyNonStaticMethodsAreUsed()
        {
            _constructorMockArtisan.DefaultValue = DefaultValue.Mock;

            // Get the class of the constructor and make sure that it only has one constructor
            Mock<ITypeElement> typeMockArtisan = Mock.Get(_constructorMockArtisan.Object.GetContainingType());
            typeMockArtisan.Setup(t => t.Constructors).Returns(new[] { _constructorMockArtisan.Object });

            // Make a mock method
            Mock<IMethod> methodMockArtisan = new Mock<IMethod>();
            methodMockArtisan.Setup(m => m.IsStatic).Returns(false);

            //Get the type to return the mock method
            typeMockArtisan.Setup(m => m.Methods).Returns(new[] { methodMockArtisan.Object });

            var result = _sut.GetCallingFunctions();

            result.Should().Contain(methodMockArtisan.Object);
        }

        [Test]
        public void DefaultConstructorsShouldBeAssessedAsLivingIfAnyNonStaticMethodsAreUsedEvenIfThereIsAlsoAStaticConstructor()
        {            
            Mock<IConstructor> staticConstructorMockArtisan = new Mock<IConstructor>();
            staticConstructorMockArtisan.Setup(s => s.IsStatic).Returns(true);
            
            // Get the class of the constructor and give it one static and one non-static constructor
            Mock<ITypeElement> typeMockArtisan = Mock.Get(_constructorMockArtisan.Object.GetContainingType());
            typeMockArtisan.Setup(t => t.Constructors).Returns(new[] { _mockConstructor, staticConstructorMockArtisan.Object });

            // Make a mock method
            Mock<IMethod> methodMockArtisan = new Mock<IMethod>();
            methodMockArtisan.Setup(m => m.IsStatic).Returns(false);

            //Get the type to return the mock method
            typeMockArtisan.Setup(m => m.Methods).Returns(new[] { methodMockArtisan.Object });

            var result = _sut.GetCallingFunctions();

            result.Should().Contain(methodMockArtisan.Object);
        }

        [Test]
        public void UncalledNonDefaultConstructorsShouldNotHaveAnyMethodsAutomaticallyRegardedAsDependingOnThem()
        {
            Mock<IConstructor> otherConstructorMockArtisan = new Mock<IConstructor>();
            otherConstructorMockArtisan.Setup(s => s.IsStatic).Returns(false);

            // Get the class of the constructor and give it two constructors
            Mock<ITypeElement> typeMockArtisan = Mock.Get(_constructorMockArtisan.Object.GetContainingType());
            typeMockArtisan.Setup(t => t.Constructors).Returns(new[] { _mockConstructor, otherConstructorMockArtisan.Object });

            // Make a mock method
            Mock<IMethod> methodMockArtisan = new Mock<IMethod>();
            methodMockArtisan.Setup(m => m.IsStatic).Returns(false);

            //Get the type to return the mock method
            typeMockArtisan.Setup(m => m.Methods).Returns(new[] { methodMockArtisan.Object });

            var result = _sut.GetCallingFunctions();

            result.Should().BeEmpty();
        }

        [Test]
        public void StaticConstructorsShouldHaveAllMethodsDependingOnThem()
        {
            // Make the constructor static
            _constructorMockArtisan.Setup(c => c.IsStatic).Returns(true);

            // Get the class of the constructor and make sure that it only has one constructor
            Mock<ITypeElement> typeMockArtisan = Mock.Get(_constructorMockArtisan.Object.GetContainingType());
            typeMockArtisan.Setup(t => t.Constructors).Returns(new[] { _mockConstructor });

            // Make a non-static mock method
            Mock<IMethod> methodMockArtisan = new Mock<IMethod>();
            methodMockArtisan.Setup(m => m.IsStatic).Returns(false);

            // Make a static mock method
            Mock<IMethod> staticMethodMockArtisan = new Mock<IMethod>();
            staticMethodMockArtisan.Setup(m => m.IsStatic).Returns(true);

            //Get the type to return the mock method
            typeMockArtisan.Setup(m => m.Methods).Returns(new[] { methodMockArtisan.Object, staticMethodMockArtisan.Object });

            var result = _sut.GetCallingFunctions();

            result.Should().Contain(methodMockArtisan.Object).And.Contain(staticMethodMockArtisan.Object);
        }
    }
}
