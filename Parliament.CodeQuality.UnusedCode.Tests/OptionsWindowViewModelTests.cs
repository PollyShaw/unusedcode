﻿using System.Collections;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using Parliament.CodeQuality.UnusedCode.Implementation.RecordsToIncludeAndIgnore;
using Parliament.CodeQuality.UnusedCode.Interfaces;
using Parliament.CodeQuality.UnusedCode.UI;

namespace Parliament.CodeQuality.UnusedCode.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [TestFixture]
    public class OptionsWindowViewModelTests
    {
        private Mock<IFunctionsToAssessSpecifier> _functionAssessorSpecifierMockArtisan;
        private OptionsWindowViewModel _sut;

        [SetUp] 
        public void SetUp()
        {
            _functionAssessorSpecifierMockArtisan = new Mock<IFunctionsToAssessSpecifier>();
        }

        [Test]
        public void CanCreateViewModel()
        {
            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);
            _sut.Should().NotBeNull();
        }

        [Test]
        public void CreatesProjects()
        {
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               new Project()
                                                                                                   {
                                                                                                       Guid = new Guid(
                                                                                                           "{57F854F9-4C54-45E9-8E8E-951C939B3197}")
                                                                                                   }
                                                                                           });
            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);


            _sut.Projects.Should().Contain(p => p.Guid == new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}"));
        }

        [Test]
        public void ProjectsAreIncludedIfInIncludedProjectsCollection()
        {
            var project = new Project()
                              {
                                  Guid = new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}")
                              };
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               project
                                                                                           });

            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.ProjectsToInclude).Returns(new Project[] { project });

            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

            _sut.Projects.Should().Contain(p => p.Guid == new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}") && p.Included);

        }

        [Test]
        public void ProjectsAreIgnoredIfInIgnoredProjectsCollection()
        {
            var project = new Project()
            {
                Guid = new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}")
            };
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               project
                                                                                           });

            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.ProjectsToIgnore).Returns(new Project[] { project });

            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

            _sut.Projects.Should().Contain(p => p.Guid == new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}") && p.Ignored);

        }

        [Test]
        public void NoErrorOccursIfIgnoredProjectIsNotInAllProjects()
        {
            var project = new Project()
            {
                Guid = new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}")
            };

            var ignoredProject = new Project()
                                     {
                                         Guid = new Guid("FF0831F4-01A1-4947-88C3-4868C3366104")
                                     };
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               project
                                                                                           });

            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.ProjectsToIgnore).Returns(new Project[] {ignoredProject });

            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

        }

        [Test]
        public void NoErrorOccursIfIncludedProjectIsNotInAllProjects()
        {
            var project = new Project()
            {
                Guid = new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}")
            };

            var includedProject = new Project()
            {
                Guid = new Guid("FF0831F4-01A1-4947-88C3-4868C3366104")
            };
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               project
                                                                                           });

            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.ProjectsToInclude).Returns(new Project[] { includedProject });

            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

        }

        [Test]
        public void ProjectNamesAreReplicatedInViewModel()
        {
            var project1 = new Project()
            {
                Guid = new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}"),
                Name = "Project 1"
            };

            var project2 = new Project()
            {
                Guid = new Guid("FF0831F4-01A1-4947-88C3-4868C3366104"),
                Name = "Project 2"
            };
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               project1,
                                                                                               project2
                                                                                           });
            
            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

            _sut.Projects.Should().Contain(p => p.Name == project1.Name).And.Contain(p => p.Name == project2.Name);

        }

        [Test]
        public void OKClickedSetsDialogResultToTrue()
        {
            _functionAssessorSpecifierMockArtisan.DefaultValue = DefaultValue.Mock;
            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

            _sut.OKClicked();

            _sut.DialogResult.Should().BeTrue();
        }

        [Test]
        public void OKClickedSetsIncludedProjects()
        {
            _functionAssessorSpecifierMockArtisan.DefaultValue = DefaultValue.Mock;
            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

            _sut.OKClicked();

            _functionAssessorSpecifierMockArtisan.Verify(f => f.SetProjectsToInclude(new Project[] { }));
            _sut.DialogResult.Should().BeTrue(); 
        }

        [Test]
        public void OKClickedSetsAProjectThatHasJustBeenIncluded()
        {
            var project1 = new Project()
            {
                Guid = new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}"),
                Name = "Project 1"
            };

            var project2 = new Project()
            {
                Guid = new Guid("FF0831F4-01A1-4947-88C3-4868C3366104"),
                Name = "Project 2"
            };
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               project1,
                                                                                               project2
                                                                                           });

            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

            _sut.Projects[0].Included = true;
            _sut.OKClicked();

            _functionAssessorSpecifierMockArtisan.Verify(f => f.SetProjectsToInclude(
                It.Is(
                (IEnumerable<Project> projectCollection) => 
                   projectCollection.Count() == 1 && projectCollection.First().Guid == _sut.Projects.First().Guid)));
            _sut.DialogResult.Should().BeTrue();
        }

        [Test]
        public void OKClickedSetsAProjectThatHasJustBeenIgnored()
        {
            var project1 = new Project()
            {
                Guid = new Guid("{57F854F9-4C54-45E9-8E8E-951C939B3197}"),
                Name = "Project 1"
            };

            var project2 = new Project()
            {
                Guid = new Guid("FF0831F4-01A1-4947-88C3-4868C3366104"),
                Name = "Project 2"
            };
            _functionAssessorSpecifierMockArtisan.SetupGet(s => s.AllProjects).Returns(new Project[]
                                                                                           {
                                                                                               project1,
                                                                                               project2
                                                                                           });

            _sut = new OptionsWindowViewModel(_functionAssessorSpecifierMockArtisan.Object);

            _sut.Projects[1].Ignored = true;
            _sut.OKClicked();

            _functionAssessorSpecifierMockArtisan.Verify(f => f.SetProjectsToIgnore(
                It.Is(
                (IEnumerable<Project> projectCollection) =>
                   projectCollection.Count() == 1 && projectCollection.First().Guid == _sut.Projects[1].Guid)));
            _sut.DialogResult.Should().BeTrue();
        }


    }
}
