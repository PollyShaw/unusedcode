﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi;
using Moq;
using Parliament.CodeQuality.UnusedCode.Implementation;

namespace Parliament.CodeQuality.UnusedCode.UI.TestRunner
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private FunctionAssessor _functionAssessor;
        private Guid _projectToIgnoreGuid;
        private Guid _projectToIncludeGuid;
        private Mock<ISolution> _solutionMockArtisan;
        private Mock<IProject> _project1MockArtisan;
        private Mock<IProject> _project2MockArtisan;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);



            _solutionMockArtisan = new Mock<ISolution>();

            _projectToIncludeGuid = new Guid("{BD504E0A-DD73-4521-BA6C-59667DDF4D3F}");
            _projectToIgnoreGuid = new Guid("{BE329110-4985-4050-A836-66B134E23301}");
           
            _project1MockArtisan = new Mock<IProject>();
            _project2MockArtisan = new Mock<IProject>();
            _project1MockArtisan.SetupGet(p => p.Guid).Returns(_projectToIncludeGuid);
            _project2MockArtisan.SetupGet(p => p.Guid).Returns(_projectToIgnoreGuid);
            _project1MockArtisan.SetupGet(p => p.Name).Returns("Project 1");
            _project2MockArtisan.SetupGet(p => p.Name).Returns("Project 2");
            _solutionMockArtisan.Setup(s => s.GetAllProjects()).Returns(new[]
                                                                               {
                                                                                   _project1MockArtisan.Object,
                                                                                   _project2MockArtisan.Object
                                                                               });

            _functionAssessor = new FunctionAssessor("ProjectsToInclude.xml", _solutionMockArtisan.Object);

            var optionsWindow = new OptionsWindow();
            optionsWindow.DataContext = new OptionsWindowViewModel(_functionAssessor);

            optionsWindow.ShowDialog();
        }
    }
}
