﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using Moq;
using NUnit.Framework;
using Parliament.CodeQuality.BuildTasks;

namespace Parliament.CodeQuality.UnusedCode.BuildTasks.Test
{
    [TestFixture(Ignore = true, IgnoreReason = "The code this is testing needs to be refactored to make it more testable because it is currently bound up with the COM interface")]
    public class CommandExecutorTests
    {
        private CommandExecutor _sut;
        private Mock<ILogger> _loggerArtisan;
        private Mock<_DTE> _visualStudio;
        private string _command;

        [SetUp]
        public void SetUp()
        {
            _loggerArtisan = new Mock<ILogger>();
            _loggerArtisan.Setup(l => l.LogError(It.IsAny<string>(), It.IsAny<object[]>()));
            _loggerArtisan.Setup(l => l.LogWarning(It.IsAny<string>(), It.IsAny<object[]>()));
            _visualStudio = new Mock<_DTE>();
            _visualStudio.DefaultValue = DefaultValue.Mock;
            _visualStudio.SetupGet(dte => dte.Solution).Returns((Solution)null);
            _visualStudio.Setup(dte => dte.ExecuteCommand(It.IsAny<string>(), It.IsAny<string>()));
            
            _sut = new CommandExecutor(_loggerArtisan.Object, _visualStudio.Object);
        }

        [Test]
        public void CanCreateLogger()
        {
            Assert.NotNull(_sut);            
        }
    
        [Test]
        public void CanCallExecute()
        {
            _sut.Execute( _command, 10);
        }

        [Test]
        public void ExecuteCallsVisualStudioExecuteCommand()
        {
            _sut.Execute(_command, 1);
            _visualStudio.Verify(dte => dte.ExecuteCommand(It.IsAny<string>(), It.IsAny<string>()));

        }

        [Test]
        public void ExecuteDoesNotLogExceptionsIfEverythingOK()
        {
            _sut.Execute(_command, 10);

            _loggerArtisan.Verify(l => l.LogError(It.IsAny<String>(), It.IsAny<object[]>()),Times.Never());

        }

        [Test]
        public void ExecuteLogs10WarningsAndOneExceptionIfCommandFails()
        {
            _visualStudio.Setup(dte => dte.ExecuteCommand(It.IsAny<string>(), It.IsAny<string>())).Throws<System.Exception>();

            _sut.Execute(_command, 10);

            _loggerArtisan.Verify(l => l.LogError(It.IsAny<String>(), It.IsAny<object[]>()), Times.Once());

            _loggerArtisan.Verify(l => l.LogWarning(It.IsAny<String>(), It.IsAny<object[]>()), Times.Exactly(10));

        }

        [Test]
        public void ExecuteLogs5WarningsAndNoExceptionsIfCommandSucceedsOnSixthAttempt()
        {
            int attempt = 1;
            _visualStudio.Setup(dte => dte.ExecuteCommand(It.IsAny<string>(), It.IsAny<string>()))
                .Callback((string s1, string s2) =>
                              {
                                  if (attempt < 6)
                                  {
                                      attempt++;
                                      throw new SystemException();
                                  }
                              });
                

            _sut.Execute(_command, 10);

            _loggerArtisan.Verify(l => l.LogWarning(It.IsAny<String>(), It.IsAny<object[]>()), Times.Exactly(5));

            _loggerArtisan.Verify(l => l.LogError(It.IsAny<String>(), It.IsAny<object[]>()), Times.Never());

        }

    }
}
