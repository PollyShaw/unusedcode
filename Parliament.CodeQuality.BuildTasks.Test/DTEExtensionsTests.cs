﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using EnvDTE;
using Moq;
using NUnit.Framework;
using Parliament.CodeQuality.BuildTasks;

namespace Parliament.CodeQuality.BuildTasks.Test
{
    [TestFixture]
    public class DTEExtensionsTests
    {
        private Mock<EnvDTE.DTE> _dteMockArtisan;
        private Mock<ILogger> _loggerMockArtisan;
        private int _maxNumberOfAttempts;



        [SetUp]
        public void SetUp()
        {
            _dteMockArtisan = new Mock<DTE>();
            _loggerMockArtisan = new Mock<ILogger>();
            _maxNumberOfAttempts = 5;

        }

        [Test]
        public void RunsFailingActionMaxRequestsNumberOfTimes()
        {
            int numberOfTimesRun = 0;
            _maxNumberOfAttempts = 2;
            try
            {
                _dteMockArtisan.Object.TryUntilSuccessOrTimeout(d =>
                                                                    {
                                                                        numberOfTimesRun++;
                                                                        throw new COMException();
                                                                    }, _maxNumberOfAttempts, _loggerMockArtisan.Object);
            }
            catch (Exception ex)
            {

            }
            Assert.AreEqual(_maxNumberOfAttempts, numberOfTimesRun);

        }

        [Test]
        public void StopsWhenErrorNotThrown()
        {
            int numberOfTimesRun = 0;
            
            _dteMockArtisan.Object.TryUntilSuccessOrTimeout(d =>
                                                                {
                                                                    numberOfTimesRun++;
                                                                    if (numberOfTimesRun >= 2)
                                                                    {
                                                                        return;
                                                                    }
                                                                    throw new COMException();
                                                                }, _maxNumberOfAttempts, _loggerMockArtisan.Object);

            Assert.AreEqual(2, numberOfTimesRun);
        }

        [Test]
        public void LogsAsMessagesWhenARetryIsGoingToBeAttempted() {
            int numberOfTimesRun = 0;
            
            _dteMockArtisan.Object.TryUntilSuccessOrTimeout(d =>
            {
                numberOfTimesRun++;
                if (numberOfTimesRun >= 2)
                {
                    return;
                }
                throw new COMException();
            }, _maxNumberOfAttempts, _loggerMockArtisan.Object);

            _loggerMockArtisan.Verify(l => l.LogMessage(It.IsAny<String>(), It.IsAny<object[]>()), Times.Exactly(1));

        }

        [Test] 
        public void DoesNotRetryIfExceptionOtherThanCOMExceptionIsThrown()
        {
            int numberOfTimesRun = 0;
            
            try
            {
                _dteMockArtisan.Object.TryUntilSuccessOrTimeout(d =>
                                                                    {
                                                                        numberOfTimesRun++;
                                                                        if (numberOfTimesRun >= 2)
                                                                        {
                                                                            return;
                                                                        }
                                                                        throw new SystemException();
                                                                    }, _maxNumberOfAttempts, _loggerMockArtisan.Object);
                Assert.Fail();

            }
            catch (SystemException ex)
            {
                Assert.AreEqual(1, numberOfTimesRun);
            }
        }

        [Test]
        public void ThrowsBuildTaskExceptionIfMaxNumberOfAttemptsIsExceeded()
        {
            try
            {

                _dteMockArtisan.Object.TryUntilSuccessOrTimeout(d =>
                                                                    {
                                                                        throw new COMException();
                                                                    }, _maxNumberOfAttempts, _loggerMockArtisan.Object);
                Assert.Fail();

            }
            catch (BuildTaskException ex)
            {
                
            }
        }

        [Test]
        public void DoesNotThrowExceptionIfExceptionTypeIsSupplied()
        {
            try
            {

                _dteMockArtisan.Object.TryUntilSuccessOrTimeout(d =>
                {
                    throw new ArgumentNullException();
                }, _maxNumberOfAttempts, _loggerMockArtisan.Object, typeof(ArgumentNullException));
                Assert.Fail();

            }
            catch (BuildTaskException ex)
            {

            }
        }
    }
}
